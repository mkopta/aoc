#!/usr/bin/env python3
from queue import PriorityQueue

LEFT, RIGHT, UP, DOWN = (0, -1), (0, 1), (-1, 0), (1, 0)


def parse_blocks(filename):
    blocks = {}
    for row, line in enumerate(open(filename), 1):
        for column, character in enumerate(line.strip(), 1):
            blocks[(row, column)] = int(character)
    return blocks, (row, column)


def determine_minimum_heat_loss(blocks, size, steps_min, steps_max):
    start = (1, 1)
    destination = (size[0], size[1])
    visited = set()
    q = PriorityQueue()
    start_state = (
        0,       # heat loss
        (1, 1),  # current block
        (0, 0),  # direction
        0        # steps in direction
    )
    q.put(start_state)
    while not q.empty():
        heat_loss, current_block, direction, steps = q.get()
        if steps >= steps_min and current_block == destination:
            return heat_loss
        if (current_block + direction + (steps,)) in visited:
            continue
        visited.add(current_block + direction + (steps,))
        for next_direction in (LEFT, RIGHT, UP, DOWN):
            if next_direction == (-direction[0], -direction[1]):
                continue  # cannot reverse direction
            if next_direction == direction and (steps + 1) > steps_max:
                continue  # at most in single direction
            if steps_min \
                        and steps < steps_min \
                        and direction != (0, 0) \
                        and next_direction != direction:
                    continue  # at minimum in single direction
            next_block = (
                current_block[0] + next_direction[0],
                current_block[1] + next_direction[1])
            if next_block not in blocks:
                continue  # out of bounds
            next_heat_loss = heat_loss + blocks[next_block]
            q.put((
                next_heat_loss,
                next_block,
                next_direction,
                (steps + 1) if direction == next_direction else 1))


blocks, size = parse_blocks(0)
print('part 1:', determine_minimum_heat_loss(blocks, size, 0, 3))
print('part 1:', determine_minimum_heat_loss(blocks, size, 4, 10))
