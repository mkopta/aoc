#!/usr/bin/env python3
from itertools import combinations


def parse_galaxies(filename):
    galaxies = set()
    for row, line in enumerate(open(filename), 1):
        for column, character in enumerate(line.strip(), 1):
            if character == '#':
                galaxies.add((row, column))
    return galaxies


def expand_universe(galaxies, expansion_constant):
    galaxies_rs = {r for r, _ in galaxies}
    galaxies_cs = {c for _, c in galaxies}
    non_galaxies_rs = {
        r for r in range(1, max(galaxies_rs) + 1) if r not in galaxies_rs}
    non_galaxies_cs = {
        c for c in range(1, max(galaxies_cs) + 1) if c not in galaxies_cs}
    expanded_galaxies = set()
    for galaxy in galaxies:
        r, c = galaxy
        r_offset = 0
        for nr in non_galaxies_rs:
            if nr < r:
                r_offset += -1 + expansion_constant
        c_offset = 0
        for nc in non_galaxies_cs:
            if nc < c:
                c_offset += -1 + expansion_constant
        expanded_galaxies.add((r + r_offset, c + c_offset))
    return expanded_galaxies


def calculate_sum_of_shortest_paths(galaxies, expansion_constant):
    galaxies = expand_universe(galaxies, expansion_constant)
    sum_of_shortest_paths = 0
    for (g1r, g1c), (g2r, g2c) in combinations(galaxies, 2):
        sum_of_shortest_paths += abs(g1r - g2r) + abs(g1c - g2c)
    return sum_of_shortest_paths


galaxies = parse_galaxies(0)
print('part 1:', calculate_sum_of_shortest_paths(galaxies, 2))
print('part 2:', calculate_sum_of_shortest_paths(galaxies, 1000000))
