#!/usr/bin/env python3


def parse_graph(filename):
    graph = {}
    for line in open(filename):
        node, connected = line.split(': ')
        if node not in graph:
            graph[node] = []
        for c in connected.strip().split(' '):
            if c not in graph[node]:
                graph[node].append(c)
            if c not in graph:
                graph[c] = []
            if node not in graph[c]:
                graph[c].append(node)
    return graph



def find_groups(graph):
    nodes = set(graph.keys())
    groups = []
    while nodes:
        init = nodes.pop()
        group = {init}
        open_nodes = {init}
        while open_nodes:
            node = open_nodes.pop()
            for c in graph[node]:
                if c in group or c in open_nodes:
                    continue
                group.add(c)
                open_nodes.add(c)
                if c in nodes:
                    nodes.remove(c)
        groups.append(group)
    return groups


graph = parse_graph(0)
nodes = set(graph.keys())
edges = {tuple(sorted((n, c))) for n, cs in graph.items() for c in cs}
print(len(find_groups(graph)))

#graph['hfx'].remove('pzl'); graph['pzl'].remove('hfx')
#graph['bvb'].remove('cmg'); graph['cmg'].remove('bvb')
#graph['nvd'].remove('jqt'); graph['jqt'].remove('nvd')
#
#print(len(find_groups(graph)))
