#!/usr/bin/env python3


def parse_schematic(filename):
    xys_to_number = {}
    xy_to_symbol = {}
    with open(filename) as f:
        for y, line in enumerate(f):
            number, xys = '', []
            for x, c in enumerate(line.strip()):
                if c.isdigit():
                    number += c
                    xys.append((x, y))
                elif number:
                    xys_to_number[tuple(xys)] = int(number)
                    number, xys = '', []
                if not c.isdigit() and c != '.':
                    xy_to_symbol[(x, y)] = c
            if number:
                xys_to_number[tuple(xys)] = int(number)
                number, xys = '', []
    return xys_to_number, xy_to_symbol


def adjacent(xy):
    x, y = xy
    return (
        (x + 1, y    ),  # →
        (x + 1, y - 1),  # ↗
        (x    , y - 1),  # ↑
        (x - 1, y - 1),  # ↖
        (x - 1, y    ),  # ←
        (x - 1, y + 1),  # ↙
        (x    , y + 1),  # ↓
        (x + 1, y + 1),  # ↘
    )


xys_to_number, xy_to_symbol = parse_schematic(0)

sum_of_part_numbers = 0
for xys, number in xys_to_number.items():
    border = set()
    for xy in xys:
        for adjxy in adjacent(xy):
            if adjxy in xys or adjxy in border:
                continue
            border.add(adjxy)
    if any(b in xy_to_symbol for b in border):
        sum_of_part_numbers += number
        continue

sum_of_gear_ratios = 0
xy_to_number = {
    xy: number
    for xys, number in xys_to_number.items()
    for xy in xys}
for xy, symbol in xy_to_symbol.items():
    if symbol != '*':
        continue
    adjnumbers = set()
    for adjxy in adjacent(xy):
        if adjxy in xy_to_number:
            adjnumbers.add(xy_to_number[adjxy])
    if len(adjnumbers) != 2:
        continue
    a, b = list(adjnumbers)
    sum_of_gear_ratios += a * b

print('part 1:', sum_of_part_numbers)
print('part 2:', sum_of_gear_ratios)
