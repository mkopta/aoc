#!/usr/bin/env python3
from enum import Enum

JOKER_RULE = False
CARD_VALUES = {'A': 14, 'K': 13, 'Q': 12, 'J': 11, 'T': 10}


def parse_hands_to_bets(filename):
    hands_to_bets = {}
    for line in open(filename):
        hand, bet = line.split()
        hands_to_bets[hand] = int(bet)
    return hands_to_bets


def card_to_number(card):
    if card.isdigit():
        return int(card)
    return CARD_VALUES[card]


class HandType(Enum):
    five_of_a_kind = 6
    four_of_a_kind = 5
    full_house = 4
    three_of_a_kind = 3
    two_pair = 2
    one_pair = 1
    high_card = 0

    def __lt__(self, other):
        return self.value < other.value


def determine_hand_type(hand):
    card_freq = {}
    for card in hand:
        card_freq[card] = card_freq.get(card, 0) + 1
    if JOKER_RULE and 'J' in hand:
        joker_count = card_freq['J']
        if joker_count == 5:
            return HandType.five_of_a_kind
        del card_freq['J']
        most_frequent_card = max(card_freq.values())
        for card, count in card_freq.items():
            if count == most_frequent_card:
                card_freq[card] += joker_count
                break
    occurences = list(card_freq.values())
    if 5 in occurences:
        return HandType.five_of_a_kind
    if 4 in occurences:
        return HandType.four_of_a_kind
    if 3 in occurences:
        if 2 in occurences:
            return HandType.full_house
        return HandType.three_of_a_kind
    if 2 in occurences:
        if occurences.count(2) == 2:
            return HandType.two_pair
        return HandType.one_pair
    return HandType.high_card


def hand_value(hand):
    hand_type = determine_hand_type(hand)
    return (hand_type,) + tuple(card_to_number(card) for card in hand)


def calculate_total_winnings(hands_to_bets):
    hands = list(hands_to_bets.keys())
    hands.sort(key=hand_value)
    total_winnings = 0
    for rank, hand in enumerate(hands, start=1):
        total_winnings += hands_to_bets[hand] * rank
    return total_winnings


hands_to_bets = parse_hands_to_bets(0)
print('part 1:', calculate_total_winnings(hands_to_bets))
CARD_VALUES['J'] = 1
JOKER_RULE = True
print('part 2:', calculate_total_winnings(hands_to_bets))
