#!/usr/bin/env python3
import itertools
from copy import copy


def count_possible_arrangements(springs, groups):
    possible_arrangements = 0
    qs = [i for i, c in enumerate(springs) if c == '?']
    for combination in itertools.product([0, 1], repeat=len(qs)):
        s = copy(springs)
        for i, c in enumerate(combination):
            s[qs[i]] = '#' if c else '.'
        g = []
        count = 0
        for c in s:
            if c == '#':
                count += 1
            elif count:
                g.append(count)
                count = 0
        if count:
            g.append(count)
        if g == groups:
            possible_arrangements += 1
    return possible_arrangements


total = 0
for line in open(0):
    a, b = line.strip().split()
    springs = list(a)
    groups = list(map(int, b.split(',')))
    total += count_possible_arrangements(springs, groups)
print(total)
