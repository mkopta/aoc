#!/usr/bin/env python3


DIRECTIONS = {
    'U': (-1, 0),
    'D': (1, 0),
    'R': (0, 1),
    'L': (0, -1)
}

def parse_instructions(filename):
    instructions = []
    for line in open(filename):
        direction, steps, color = line.split()
        instructions.append(
            (DIRECTIONS[direction], int(steps), color[2:-1])
        )
    return instructions


def print_blocks(blocks):
    rs = [r for r, _ in blocks]
    cs = [c for _, c in blocks]
    for r in range(min(rs), max(rs) + 1):
        for c in range(min(cs), max(cs) + 1):
            if (r, c) in blocks:
                print('#', end='')
            else:
                print('.', end='')
        print()


def dig_trench(instructions):
    trench = {(0, 0)}
    r, c = (0, 0)
    for (dr, dc), steps, _ in instructions:
        for _ in range(steps):
            r, c = r + dr, c + dc
            trench.add((r, c))
    return trench


def dig_lagoon(trench):
    rs = [r for r, _ in trench]
    cs = [c for _, c in trench]
    min_r, max_r = min(rs) - 1, max(rs) + 1
    min_c, max_c = min(cs) - 1, max(cs) + 1
    init = (min_r, min_c)
    outside = {init}
    flowing = [init]
    while flowing:
        r, c = flowing.pop(0)
        for dr, dc in DIRECTIONS.values():
            nr, nc = r + dr, c + dc
            if not (min_r <= nr <= max_r) or not (min_c <= nc <= max_c):
                continue
            if (nr, nc) in outside or (nr, nc) in trench:
                continue
            outside.add((nr, nc))
            flowing.append((nr, nc))
    return {
        (r, c)
        for r in range(min_r, max_r + 1)
        for c in range(min_c, max_c + 1)
        if (r, c) not in outside}


def shoelace(vertices):
    first_part, second_part = 0, 0
    for i in range(len(vertices)):
        first_part += vertices[i][0] * vertices[(i + 1) % len(vertices)][1]
        second_part += vertices[i][1] * vertices[(i + 1) % len(vertices)][0]
    return abs(first_part - second_part) // 2
        

instructions = parse_instructions(0)
trench = dig_trench(instructions)
lagoon = dig_lagoon(trench)
print('part 1 (flood-fill):', len(lagoon))

vertices = [(0, 0)]
r, c = (0, 0)
for (dr, dc), steps, _ in instructions:
    r, c = r + dr * steps, c + dc * steps
    vertices.append((r, c))
perimeter = sum(s for _, s, __ in instructions)
print('part 1 (shoelace):', shoelace(vertices) + perimeter // 2 + 1)

HEXDIR = {'0': 'R', '1': 'D', '2': 'L', '3': 'U'}
vertices = [(0, 0)]
r, c = (0, 0)
perimeter = 0
for *_, color in instructions:
    dr, dc = DIRECTIONS[HEXDIR[color[-1]]]
    steps = int(color[:-1], 16)
    r, c = r + dr * steps, c + dc * steps
    vertices.append((r, c))
    perimeter += steps
print('part 2 (shoelace):', shoelace(vertices) + perimeter // 2 + 1)
