#!/usr/bin/env python3
from copy import deepcopy


def parse_bricks(filename):
    bricks = {}
    for brick_id, line in enumerate(open(filename)):
        brick = set()
        a, b = line.strip().split('~')
        ax, ay, az = map(int, a.split(','))
        bx, by, bz = map(int, b.split(','))
        brick.add((ax, ay, az))
        while (ax, ay, az) != (bx, by, bz):
            if ax < bx:
                ax += 1
            elif ay < by:
                ay += 1
            elif az < bz:
                az += 1
            brick.add((ax, ay, az))
        bricks[brick_id] = brick
    return bricks


def move_brick_down(brick):
    fallen_brick = set()
    for x, y, z in brick:
        fallen_brick.add((x, y, z - 1))
    return fallen_brick


def is_on_ground(brick):
    return any(z == 1 for *_, z in brick)


def collides(brick, bricks):
    for other_brick in bricks:
        for xyz in brick:
            if xyz in other_brick:
                return True
    return False


def fallstep(bricks):
    moved = set()
    for brick_id, brick in bricks.items():
        if is_on_ground(brick):
            continue
        fallen_brick = move_brick_down(brick)
        if not collides(
                fallen_brick,
                [b for bid, b in bricks.items() if bid != brick_id]):
            bricks[brick_id] = fallen_brick
            moved.add(brick_id)
    return moved


def fall(bricks):
    all_moved = set()
    while currently_moved := fallstep(bricks):
        all_moved |= currently_moved
    return all_moved


def count_disintegrable_bricks(bricks):
    count = 0
    bricks_copy = deepcopy(bricks)
    for brick_id in list(bricks):
        del bricks[brick_id]
        if not fallstep(bricks):
            count += 1
        bricks = deepcopy(bricks_copy)
    return count


def count_chains(bricks):
    count = 0
    bricks_copy = deepcopy(bricks)
    for brick_id in list(bricks):
        del bricks[brick_id]
        count += len(fall(bricks))
        bricks = deepcopy(bricks_copy)
    return count


bricks = parse_bricks(0)
fall(bricks)
print('part 1:', count_disintegrable_bricks(deepcopy(bricks)))
print('part 2:', count_chains(bricks))
