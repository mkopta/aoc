#!/usr/bin/env python3
from queue import PriorityQueue


def parse_tiles(filename):
    tiles, start = {}, None
    for row, line in enumerate(open(filename)):
        for column, character in enumerate(line.strip()):
            tiles[(row, column)] = character
            if character == 'S':
                start = (row, column)
    return tiles, start


def nicer(c):
    return {
        '.': ' ',
        '-': '═',
        '|': '║',
        'L': '╚',
        '7': '╗',
        'J': '╝',
        'F': '╔'
    }[c]


def print_tiles(tiles, inner):
    rs, cs = [r for r, _ in tiles], [c for _, c in tiles]
    for r in range(min(rs), max(rs) + 1):
        for c in range(min(cs), max(cs) + 1):
            if (r, c) in inner:
                print(f'\033[7m{nicer(tiles[(r, c)])}\033[0m', end='')
            else:
                print(nicer(tiles[(r, c)]), end='')
        print()


def expand(rc, tiles):
    """ - 7 L F J | """
    r, c = rc
    left  = r    , c - 1
    right = r    , c + 1
    up    = r - 1, c
    down  = r + 1, c
    if tiles[rc] in {'S', '-', '7', 'J'} \
            and tiles.get(left) in ('-', 'F', 'L'):
        yield left
    if tiles[rc] in {'S', '-', 'F', 'L'} \
            and tiles.get(right) in ('-', '7', 'J'):
        yield right
    if tiles[rc] in {'S', 'L', 'J', '|'} \
            and tiles.get(up) in ('|', '7', 'F'):
        yield up
    if tiles[rc] in {'S', '7', 'F', '|'} \
            and tiles.get(down) in ('|', 'L', 'J'):
        yield down


def part1(tiles, start):
    open_rcs = PriorityQueue()
    loop = {start}
    farthest_from_start = 0
    open_rcs.put((0, start))
    while not open_rcs.empty():
        dist, rc = open_rcs.get()
        for new_rc in expand(rc, tiles):
            if new_rc in loop:
                continue
            open_rcs.put((dist + 1, new_rc))
            loop.add(new_rc)
            farthest_from_start = max(dist + 1, farthest_from_start)
    return farthest_from_start, loop


def part2(tiles, loop, start):
    # clear junk
    rs, cs = [r for r, _ in tiles], [c for _, c in tiles]
    for r in range(max(rs) + 1):
        for c in range(max(cs) + 1):
            if (r, c) not in loop:
                tiles[(r, c)] = '.'
    # replace start
    r, c = start
    left  = r    , c - 1
    right = r    , c + 1
    up    = r - 1, c
    down  = r + 1, c
    start_neighbors = {rc for rc in expand(start, tiles)}
    if {left, right} == start_neighbors:
        tiles[start] = '-'
    elif {up, down} == start_neighbors:
        tiles[start] = '|'
    elif {right, down} == start_neighbors:
        tiles[start] = 'F'
    elif {up, right} == start_neighbors:
        tiles[start] = 'L'
    elif {up, left} == start_neighbors:
        tiles[start] = 'J'
    elif {left, down} == start_neighbors:
        tiles[start] = '7'
    # find inner
    inner_tiles_count = 0
    inner_tiles = set()
    for r in range(max(rs) + 1):
        inner = False
        for c in range(max(cs) + 1):
            if tiles[(r, c)] in ('|', 'F', '7'):
                inner = not inner
            elif tiles[(r, c)] == '.' and inner:
                inner_tiles.add((r, c))
                inner_tiles_count += 1
    print_tiles(tiles, inner_tiles)
    return inner_tiles_count


tiles, start = parse_tiles(0)
farthest_from_start, loop = part1(tiles, start)
print('part 1:', farthest_from_start)
print('part 2:', part2(tiles, loop, start))
