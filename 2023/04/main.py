#!/usr/bin/env python3
import math


def parse_cards(filename):
    cards = {}
    for line in open(0):
        card_number, winning, have = parse_card(line)
        cards[card_number] = (winning, have)
    return cards


def parse_card(line):
    a, b = line.split(':')
    _, card_number = a.strip().split()
    card_number = int(card_number)
    winning, have = b.strip().split('|')
    winning = set(map(int, winning.split()))
    have = list(map(int, have.split()))
    return card_number, winning, have


def calculate_number_of_matches(cards):
    number_of_matches = {}
    for card_number, (winning, have) in cards.items():
        matches = 0
        for n in have:
            if n in winning:
                matches += 1
        number_of_matches[card_number] = matches
    return number_of_matches


def part1(matches):
    return sum(
        int(math.pow(2, matches - 1))
        for matches in number_of_matches.values()
        if matches > 0)


def evaluate(card_number, cards, number_of_matches, _cache={}):
    if card_number in _cache:
        return _cache[card_number]
    matches = number_of_matches[card_number]
    added_cards_count = 0
    for copy in (card_number + i for i in range(1, matches + 1)):
        added_cards_count += 1 + evaluate(copy, cards, number_of_matches)
    _cache[card_number] = added_cards_count
    return added_cards_count


def part2(cards, number_of_matches):
    to_evaluate = list(cards)
    total_number_of_cards = 0
    while to_evaluate:
        card_number = to_evaluate.pop(0)
        total_number_of_cards += \
            1 + evaluate(card_number, cards, number_of_matches)
    return total_number_of_cards


cards = parse_cards(0)
number_of_matches = calculate_number_of_matches(cards)
print('part 1:', part1(number_of_matches))
print('part 2:', part2(cards, number_of_matches))
