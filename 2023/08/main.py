#!/usr/bin/env python3
from itertools import cycle
from functools import reduce


def parse_map(filename):
    nodes = {}
    instructions, lines = open(0).read().split('\n\n')
    for line in lines.strip().split('\n'):
        node, rest = line.split(' = ')
        a, b = rest.replace('(', '').replace(')', '').strip().split(', ')
        nodes[node] = (a, b)
    return instructions, nodes


def count_steps_to_Z(instructions, nodes, init):
    steps = 0
    node = init
    for instruction in cycle(instructions):
        steps += 1
        if instruction == 'L':
            node = nodes[node][0]
        elif instruction == 'R':
            node = nodes[node][1]
        if node.endswith('Z'):
            break
    return steps


def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def lcm(a, b):
    return (a * b) // gcd(a, b)


def part2(instruction, nodes):
    init_nodes = [n for n in nodes if n.endswith('A')]
    counts = [
        count_steps_to_Z(instructions, nodes, node) for node in init_nodes]
    return reduce(lcm, counts)


instructions, nodes = parse_map(0)
print('part 1:', count_steps_to_Z(instructions, nodes, 'AAA'))
print('part 2:', part2(instructions, nodes))
