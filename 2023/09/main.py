#!/usr/bin/env python3

def predict_values(sequence):
    if all(n == 0 for n in sequence):
        return 0, 0
    new_sequence = [b - a for a, b in zip(sequence, sequence[1:])]
    prev_value, next_value = predict_values(new_sequence)
    return sequence[0] - prev_value, sequence[-1] + next_value


predictions = [
    predict_values(list(map(int, line.split())))
    for line in open(0)
]
print('part 1:', sum(p[1] for p in predictions))
print('part 2:', sum(p[0] for p in predictions))
