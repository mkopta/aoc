#!/usr/bin/env python3


def parse_map(filename):
    start = None
    garden_plots = set()
    rocks = set()
    for r, line in enumerate(open(filename), 1):
        for c, char in enumerate(line.strip(), 1):
            if char == '#':
                rocks.add((r, c))
            elif char == '.':
                garden_plots.add((r, c))
            elif char == 'S':
                start = (r, c)
                garden_plots.add((r, c))
    return start, garden_plots, rocks, (r, c)
            

start, garden_plots, rocks, dimensions = parse_map(0)
reached = {start}
for _ in range(64):
    reached_next = set()
    while reached:
        r, c = reached.pop()
        for dr, dc in ((-1, 0), (1, 0), (0, -1), (0, 1)):
            nr, nc = r + dr, c + dc
            if (nr, nc) in rocks or (nr, nc) not in garden_plots:
                continue
            reached_next.add((nr, nc))
    reached = reached_next
print('part 1:', len(reached))
