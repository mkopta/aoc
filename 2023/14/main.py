#!/usr/bin/env python3

def parse_platform(filename):
    round_rocks, square_rocks = set(), set()
    rows_count, columns_count = 0, 0
    for row, line in enumerate(open(filename), 1):
        line = line.strip()
        for column, character in enumerate(line, 1):
            if character == '#':
                square_rocks.add((row, column))
            elif character == 'O':
                round_rocks.add((row, column))
            columns_count = max(columns_count, column)
        rows_count += 1
    return (rows_count, columns_count), round_rocks, square_rocks


def print_platform(dimensions, round_rocks, square_rocks):
    rows_count, columns_count = dimensions
    for r in range(1, rows_count + 1):
        for c in range(1, columns_count + 1):
            if (r, c) in square_rocks:
                print('#', end='')
            elif (r, c) in round_rocks:
                print('O', end='')
            else:
                print('.', end='')
        print()


def roll_rock(dimensions, round_rocks, square_rocks, rock, direction):
    r, c = rock
    dr, dc = direction
    rows_count, columns_count = dimensions
    if not (1 <= (r + dr) <= rows_count) \
            or not (1 <= (c + dc) <= columns_count):
        return False  # out of bounds
    if (r + dr, c + dc) in square_rocks \
            or (r + dr, c + dc) in round_rocks:
        return False  # obstacle
    round_rocks.remove((r, c))
    round_rocks.add((r + dr, c + dc))
    return True


def roll_rocks(dimensions, round_rocks, square_rocks, direction):
    rocks_moved = False
    rows_count, columns_count = dimensions
    for r in range(1, rows_count + 1):
        for c in range(1, columns_count + 1):
            rock = (r, c)
            if rock not in round_rocks:
                continue
            rocks_moved |= roll_rock(
                dimensions, round_rocks, square_rocks, rock, direction)
    return rocks_moved


def tilt(dimensions, round_rocks, square_rocks, direction):
    while roll_rocks(dimensions, round_rocks, square_rocks, direction):
        pass


def calculate_north_load(dimensions, round_rocks):
    load = 0
    rows_count, columns_count = dimensions
    for r in range(1, rows_count + 1):
        count_in_row = 0
        for c in range(1, columns_count + 1):
            rock = (r, c)
            if rock not in round_rocks:
                continue
            count_in_row += 1
        load += (rows_count + 1 - r) * count_in_row
    return load


north, west, south, east = (-1, 0), (0, -1), (1, 0), (0, 1)
dimensions, round_rocks, square_rocks = parse_platform(0)
tilt(dimensions, round_rocks, square_rocks, (-1, 0))
#print_platform(dimensions, round_rocks, square_rocks)
print('part 1:', calculate_north_load(dimensions, round_rocks))
