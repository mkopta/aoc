#!/usr/bin/env python3

LEFT = (0, -1)
RIGHT = (0, 1)
UP = (-1, 0)
DOWN = (1, 0)


def parse_mirrors():
    mirrors = {}
    for r, line in enumerate(open(0), 1):
        for c, char in enumerate(line.strip(), 1):
            if char in ('|', '-', '/', '\\'):
                mirrors[(r, c)] = char
    dimensions = (r, c)
    return mirrors, dimensions


def count_energized_tiles(mirrors, dimensions, init, direction):
    max_r, max_c = dimensions
    energized_tiles = {init}
    moving_beams = [init + direction]
    visited = set()
    for beam in moving_beams:
        r, c, dr, dc = beam
        visited.add((r, c, dr, dc))
        energized_tiles.add((r, c))
        mirror = mirrors.get((r, c))
        if not mirror \
                or (mirror == '|' and not dc) \
                or (mirror == '-' and not dr):
            new_directions = [(dr, dc)]  # continuing in a straight line
        elif mirror == '|':
            new_directions = [UP, DOWN]
        elif mirror == '-':
            new_directions = [LEFT, RIGHT]
        elif mirror == '/':
            new_directions = [(-dc, -dr)]
        elif mirror == '\\':
            new_directions = [(dc, dr)]
        for dr, dc in new_directions:
            nr, nc = r + dr, c + dc
            if not (1 <= nr <= max_r and 1 <= nc <= max_c):
                continue  # out of bounds
            if (nr, nc, dr, dc) in visited:
                continue  # stop the cycle
            moving_beams.append((nr, nc, dr, dc))
    return len(energized_tiles)


def part2(mirrors, dimensions):
    max_energized = 0
    max_r, max_c = dimensions
    for r in range(1, max_r + 1):  # left and right edge
        for c, direction in ((1, RIGHT), (max_c, LEFT)):
            max_energized = max(
                max_energized,
                count_energized_tiles(mirrors, dimensions, (r, c), direction))
    for c in range(1, max_c + 1):  # top and bottom edge
        for r, direction in ((1, DOWN), (max_r, UP)):
            max_energized = max(
                max_energized,
                count_energized_tiles(mirrors, dimensions, (r, c), direction))
    return max_energized


mirrors, dimensions = parse_mirrors()
print('part 1:', count_energized_tiles(mirrors, dimensions, (1, 1), RIGHT))
print('part 2:', part2(mirrors, dimensions))
