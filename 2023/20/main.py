#!/usr/bin/env python3
from copy import deepcopy
from functools import reduce

LOW = 'low'
HIGH = 'high'

class Reached(Exception):
    pass


def parse_modules():
    modules = {}
    for line in open(0):
        src, destinations = line.strip().split(' -> ')
        src_type = src[0] if src[0] in ('%', '&') else None
        src_name = src[1:] if src_type else src
        if src_name == 'broadcaster':
            src_type = '*'
        destinations = destinations.split(', ')
        module = {
            'name': src_name,
            'type': src_type,
            'destinations': destinations}
        if src_type == '%':
            module['state'] = 'off'
        elif src_type == '&':
            module['recent_pulses_in'] = {}
        modules[src_name] = module
    # find conjunctions input modules
    for module in modules.values():
        if module['type'] == '&':
            for m in modules.values():
                if module['name'] in m['destinations']:
                    module['recent_pulses_in'][m['name']] = LOW
    return modules


def press_button(modules, until=None):
    count = {HIGH: 0, LOW: 0}
    pulses_to_resolve = [('button', 'low', 'broadcaster')]
    count[LOW] += 1
    while pulses_to_resolve:
        from_module_name, signal, module_name = pulses_to_resolve.pop(0)
        if until and until == (from_module_name, signal, module_name):
            raise Reached()
        if module_name not in modules:
            continue
        module = modules[module_name]
        if module['type'] == '*':
            for dest in module['destinations']:
                pulses_to_resolve.append((module_name, signal, dest))
                count[signal] += 1
        elif module['type'] == '%':
            if signal == 'high':
                continue
            if module['state'] == 'off':
                module['state'] = 'on'
                signal = HIGH
            elif module['state'] == 'on':
                module['state'] = 'off'
                signal = LOW
            for dest in module['destinations']:
                pulses_to_resolve.append((module_name, signal, dest))
                count[signal] += 1
        elif module['type'] == '&':
            module['recent_pulses_in'][from_module_name] = signal
            if all(signal == HIGH for signal in module['recent_pulses_in'].values()):
                signal = LOW
            else:
                signal = HIGH
            for dest in module['destinations']:
                pulses_to_resolve.append((module_name, signal, dest))
                count[signal] += 1
    return count


def part1(modules):
    count = {HIGH: 0, LOW: 0}
    for _ in range(1000):
        c = press_button(modules)
        count[HIGH] += c[HIGH]
        count[LOW] += c[LOW]
    return count[LOW] * count[HIGH]


def steps_until(modules, pulse):
    steps = 0
    while True:
        try:
            steps += 1
            press_button(modules, until=pulse)
        except Reached:
            return steps


def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def lcm(a, b):
    return (a * b) // gcd(a, b)


def part2(modules):
    modules_backup = deepcopy(modules)
    for module in modules.values():
        if module['destinations'] == ['rx']:
            intermediate = module['name']
            break
    else:
        raise Exception('eh..')
    chevrons = modules[intermediate]['recent_pulses_in']
    steps = []
    for chevron in chevrons:
        modules = deepcopy(modules_backup)
        steps.append(steps_until(modules, (chevron, HIGH, intermediate)))
    return reduce(lcm, steps)


modules = parse_modules()
modules_backup = deepcopy(modules)
print('part 1:', part1(modules))
modules = deepcopy(modules_backup)
print('part 2:', part2(modules))
