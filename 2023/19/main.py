#!/usr/bin/env python3


def parse(filename):
    flows, parts = {}, []
    a, b = open(filename).read().split('\n\n')
    for line in a.strip().split('\n'):
        flow_name, rest = line.split('{')
        rules = []
        for rule in rest[:-1].split(','):
            if ':' in rule:
                condition, destination = rule.split(':')
                if '>' in condition:
                    a, val = condition.split('>')
                    condition = (a, '>', int(val))
                else:
                    a, val = condition.split('<')
                    condition = (a, '<', int(val))
            else:
                condition, destination = None, rule
            rules.append((condition, destination))
        flows[flow_name] = rules
    for line in b.strip().split('\n'):
        part = {}
        attrs = line[1:-1].split(',')
        for attr in attrs:
            f, v = attr.split('=')
            part[f] = int(v)
        parts.append(part)
    return flows, parts


def is_accepted(part, flows):
    flow = 'in'
    while True:
        if flow == 'A':
            return True
        if flow == 'R':
            return False
        for (cond, dest) in flows[flow]:
            if not cond:
                flow = dest
                break
            f, op, val = cond
            if (op == '>' and part[f] > val) or (op == '<' and part[f] < val):
                flow = dest
                break


def part2(flows):
    accepted = []
    states = []
    # state = (flow, xrange, mrange, arange, srange)
    index = {'x': 0, 'm': 1, 'a': 2, 's': 3}
    init = ('in', (1, 4000), (1, 4000), (1, 4000), (1, 4000))
    states.append(init)
    while states:
        flow, *ranges = states.pop(0)
        if flow == 'A':
            accepted.append(ranges)
            continue
        if flow == 'R':
            continue
        ranges = list(ranges)
        for (cond, dest) in flows[flow]:
            # e.g. px{a<2006:qkq,m>2090:A,rfg}
            if not cond:
                states.append((dest,) + tuple(ranges))
                continue
            f, op, val = cond
            since, until = ranges[index[f]]
            if op == '<':
                if until < val:
                    states.append((dest,) + tuple(ranges))
                    break
                elif since < val and until >= val:
                    ranges[index[f]] = (since, val - 1)
                    states.append((dest,) + tuple(ranges))
                    ranges[index[f]] = (val, until)
            elif op == '>':
                if since >= val:
                    states.append((dest,) + tuple(ranges))
                    break
                elif since < val and until >= val:
                    ranges[index[f]] = (val + 1, until)
                    states.append((dest,) + tuple(ranges))
                    ranges[index[f]] = (since, val)
    total = 0
    for ranges in accepted:
        count = 1
        for a, b in ranges:
            count *= (b - a) + 1
        total += count
    return total



flows, parts = parse(0)
total = 0
for part in parts:
    if is_accepted(part, flows):
        total += sum(part.values())
print('part 1:', total)
print('part 2:', part2(flows))
