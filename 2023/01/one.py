#!/usr/bin/env python3
total = 0
for line in open(0):
    numbers = [c for c in line if c.isdigit()]
    total += int(numbers[0] + numbers[-1])
print(total)
