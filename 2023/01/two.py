#!/usr/bin/env python3

numbers = 'one two three four five six seven eight nine'.split()

def get_first_number(line):
    while line:
        for index, number in enumerate(numbers):
            if line.startswith(number):
                return str(index + 1)
        if line[0].isdigit():
            return line[0]
        line = line[1:]

def get_last_number(line):
    while line:
        for index, number in enumerate(numbers):
            if line.endswith(number):
                return str(index + 1)
        if line[-1].isdigit():
            return line[-1]
        line = line[:-1]

total = 0
for line in open(0):
    n1 = get_first_number(line)
    n2 = get_last_number(line)
    total += int(n1 + n2)
print(total)
