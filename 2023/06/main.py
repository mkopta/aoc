#!/usr/bin/env python3
import re
import math


def parse_input(filename):
    with open(filename) as f:
        line1, line2 = f.read().splitlines()
        time_limits = list(map(int, re.findall(r'\d+', line1)))
        max_distances = list(map(int, re.findall(r'\d+', line2)))
    return time_limits, max_distances


def calc_number_of_ways_to_beat_the_record(time_limit, max_distance):
    number_of_ways_to_beat_the_record = 0
    for hold_time in range(1, time_limit):
        speed = hold_time
        move_time = time_limit - hold_time
        distance = speed * move_time
        if distance > max_distance:
            number_of_ways_to_beat_the_record += 1
    return number_of_ways_to_beat_the_record


def part1(time_limits, max_distances):
    numbers_of_ways = []
    for time_limit, max_distance in zip(time_limits, max_distances):
        numbers_of_ways.append(
            calc_number_of_ways_to_beat_the_record(time_limit, max_distance))
    return math.prod(numbers_of_ways)


def part2(time_limits, max_distances):
    """ bruteforce """
    time_limit = int(''.join(map(str, time_limits)))
    max_distance = int(''.join(map(str, max_distances)))
    return calc_number_of_ways_to_beat_the_record(time_limit, max_distance)


def part2q(time_limits, max_distances):
    """
    using quadratic equation
    https://www.matweb.cz/kvadraticke-nerovnice/
    """
    time_limit = int(''.join(map(str, time_limits)))
    max_distance = int(''.join(map(str, max_distances)))
    """
    speed = hold_time
    move_time = time_limit - hold_time
    distance = speed * move_time
        distance > max_distance
        hold_time * (time_limit - hold_time) > max_distance
        -1*hold_time^2 + time_limit*hold_time + -1*max_distance > 0
    ax^2 + bx + c = 0
    a = -1
    x = hold_time
    b = time_limit
    c = -1*max_distance
    """
    D = time_limit ** 2 - 4 * -1 * (-1 * max_distance)
    x1 = math.ceil((-time_limit + math.sqrt(D)) / (2 * -1))
    x2 = math.floor((-time_limit - math.sqrt(D)) / (2 * -1))
    return x2 - x1 + 1


time_limits, max_distances = parse_input(0)
print('part 1:', part1(time_limits, max_distances))
#print('part 2:', part2(time_limits, max_distances))
print('part 2:', part2q(time_limits, max_distances))
