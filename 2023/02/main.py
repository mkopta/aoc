#!/usr/bin/env python
import re
import math

LIMITS = {'red': 12, 'green': 13, 'blue': 14}
GAME_ID_PROG = re.compile(r'^Game (?P<id>[0-9]+):')


def parse_game(hands):
    game = []
    # e.g. 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    for hand in hands.split(';'):
        iteration = {}
        # e.g. 3 blue, 4 red
        for cubes in hand.strip().split(','):
            # e.g. 3 blue
            count_str, color = cubes.split()
            iteration[color] = int(count_str)
        game.append(iteration)
    return game


def parse_games(filename):
    games = {}
    with open(filename) as f:
        for line in f:
            line = line.strip()
            m = GAME_ID_PROG.search(line)
            game_id = int(m.group('id'))
            hands = line.split(':')[1]
            games[game_id] = parse_game(hands)
    return games


def is_valid_game(game):
    for hand in game:
        for color, count in hand.items():
            if LIMITS[color] < count:
                return False
    return True


def minimum_cubes(game):
    minimum = {}
    for hand in game:
        for color, count in hand.items():
            minimum[color] = max(minimum.get(color, 0), count)
    return minimum


part1_total, part2_total = 0, 0
games = parse_games(0)
for game_id, game in games.items():
    if is_valid_game(game):
        part1_total += game_id
    minimum = minimum_cubes(game)
    part2_total += math.prod(minimum.values())
print('part 1:', part1_total)
print('part 2:', part2_total)
