#!/usr/bin/env python3


def parse_input(filename):
    seeds, *blocks = open(filename).read().split('\n\n')
    seeds = list(map(int, seeds.split()[1:]))
    maps = []
    for block in blocks:
        lines = block.strip().split('\n')
        maps.append([[int(n) for n in line.split()] for line in lines[1:]])
    return seeds, maps


def part1(seeds, maps):
    lowest_location = max(dest for dest, *_ in maps[-1])
    for seed in seeds:
        n = seed
        for a_map in maps:
            for dest, src, rlen in a_map:
                if src <= n < (src + rlen):
                    n = dest + (n - src)
                    break
        location = n
        lowest_location = min(lowest_location, location)
    return lowest_location


def part2(seeds_ranges, maps):
    lowest_location = max(dest for dest, *_ in maps[-1])
    while seeds_ranges:
        base, rlen, *seeds_ranges = seeds_ranges  # take first seeds range
        ranges = [(base, base + rlen)]  # <a, b)
        for a_map in maps:
            new_ranges = []
            while ranges:
                a_range = ranges.pop()
                for dest, src, rlen in a_map:
                    offset = dest - src
                    if (a_range[1] - 1) < src or a_range[0] >= (src + rlen):
                        continue  # a_range lies outside this mapping
                    if src <= a_range[0] and (a_range[1] - 1) < (src + rlen):
                        # a_range lies within this mapping
                        new_ranges.append(
                            (a_range[0] + offset, a_range[1] + offset))
                        break
                    if src <= a_range[0] and (a_range[1] - 1) >= (src + rlen):
                        # overlaps on right
                        new_ranges.append(
                            (a_range[0] + offset, src + rlen + offset))
                        ranges.append((src + rlen, a_range[1]))
                        break
                    if src > a_range[0] and (a_range[1] - 1) < (src + rlen):
                        # overlaps on left
                        new_ranges.append((src + offset, a_range[1] + offset))
                        ranges.append((a_range[0], src))
                        break
                else:
                    # there is no mapping for this range, it's kept as is
                    new_ranges.append(a_range)
            ranges = new_ranges
        lowest_location = min(lowest_location, min(r[0] for r in ranges))
    return lowest_location


seeds, maps = parse_input(0)
print('part 1:', part1(seeds, maps))
print('part 2:', part2(seeds, maps))
