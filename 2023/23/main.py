#!/usr/bin/env python3
from copy import copy
from collections import deque


def parse_map(filename):
    the_map, start, end = {}, None, None
    for row, line in enumerate(open(filename), 1):
        for col, char in enumerate(line.strip(), 1):
            the_map[(row, col)] = char
            if char == '.':
                if not start:
                    start = (row, col)
                end = (row, col)
    return the_map, start, end, (row, col)


def find_max_hike_length(the_map, start, end, dimensions):
    reached = {rc: 0 for rc in the_map if the_map[rc] == '.'}
    max_hike_length = 0
    hikes_in_progress = deque([(start, {start})])
    # hike_in_progress = (current_position, {visited_positions})
    while hikes_in_progress:
        current, visited = hikes_in_progress.pop()
        r, c = current
        if the_map[current] == '<':
            options = ((0, -1),)
        elif the_map[current] == '>':
            options = ((0, 1),)
        elif the_map[current] == 'v':
            options = ((1, 0),)
        elif the_map[current] == '^':
            options = ((-1, 0),)
        else:  # the_map[current] == '.':
            options = ((-1, 0), (1, 0), (0, -1), (0, 1))
        for dr, dc in options:
            next_position = nr, nc = r + dr, c + dc
            if next_position in visited \
                    or next_position not in the_map \
                    or the_map[next_position] == '#':
                continue
            if next_position == end:
                max_hike_length = max(max_hike_length, len(visited))
                continue
            if reached.get(next_position, 0) > (len(visited) + 1):
                continue  # pruning
            reached[next_position] = len(visited) + 1
            next_visited = copy(visited)
            next_visited.add(next_position)
            hikes_in_progress.append((next_position, next_visited))
    return max_hike_length


the_map, start, end, dimensions = parse_map(0)
print('part 1:', find_max_hike_length(the_map, start, end, dimensions))
for rc in the_map:
    if the_map[rc] in ('<', '>', '^', 'v'):
        the_map[rc] = '.'
print('part 2:', find_max_hike_length(the_map, start, end, dimensions))
# 4710 is too low
