#!/usr/bin/env python3

def parse_patterns(filename):
    patterns = []
    for pattern in open(0).read().split('\n\n'):
        patterns.append(pattern.split())
    return patterns


def find_reflections_in_line(line):
    reflections = set()
    for i in range(1, len(line)):
        left = line[:i]
        right = line[i:]
        if len(left) < len(right):
            right = right[:len(left)]
        elif len(left) > len(right):
            left = left[len(left)-len(right):]
        if left == right[::-1]:
            reflections.add(i)
    return reflections


def find_perfect_vertical_reflection(pattern):
    reflections = find_reflections_in_line(pattern[0])
    for row in pattern[1:]:
        reflections &= find_reflections_in_line(row)
    return list(reflections)[0] if reflections else 0


def find_perfect_horizontal_reflection(pattern):
    reflections = set()
    for column in range(len(pattern[0])):
        line = ''  # vertical line
        for row in range(len(pattern)):
            line += pattern[row][column]
        r = find_reflections_in_line(line)
        if not reflections:
            reflections = r
        else:
            reflections &= r
    if not reflections:
        return None
    assert len(reflections) == 1
    return list(reflections)[0]


def find_perfect_reflection(pattern):
    vertical_reflection = find_perfect_vertical_reflection(pattern)
    if vertical_reflection:
        return vertical_reflection, 0
    return 0, find_perfect_horizontal_reflection(pattern)


total = 0
for pattern in parse_patterns(0):
    c, r = find_perfect_reflection(pattern)
    total += c + 100 * r
print(total)
