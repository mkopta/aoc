#!/usr/bin/env python3

def HASH(string):
    current_value = 0
    for c in string:
        current_value += ord(c)
        current_value *= 17
        current_value %= 256
    return current_value


steps = open(0).read().strip().split(',')
print('part 1:', sum(map(HASH, steps)))
boxes = {i: [] for i in range(256)}
for step in steps:
    if step.endswith('-'):
        operation = '-'
        lens_label = step[:-1]
        box = boxes[HASH(lens_label)]
        for i, (_lens_label, _) in enumerate(box):
            if lens_label == _lens_label:
                del box[i]
                break
    else:
        operation = '='
        lens_label, focal_length = step.split('=')
        focal_length = int(focal_length)
        box = boxes[HASH(lens_label)]
        for i, (_lens_label, _) in enumerate(box):
            if lens_label == _lens_label:
                box[i] = (lens_label, focal_length)
                break
        else:
            box.append((lens_label, focal_length))
    #print(f'After "{step}"')
    #for box_number, box in boxes.items():
    #    if box:
    #        print(f'Box {box_number}: ', end='')
    #        for lens, focal_length in box:
    #            print(f'[{lens} {focal_length}] ', end='')
    #        print()
    #print()
focusing_power = 0
for box_number, box in boxes.items():
    for i, (lens_label, focal_length) in enumerate(box, 1):
        focusing_power += (box_number + 1) * i * focal_length
print('part 2:', focusing_power)
