#!/usr/bin/env python3
# Rock, Paper, Scissors
#  A      B       C     (opponent)
#  X      Y       Z     (me)
scoring = {
    'A X': 1 + 3,
    'A Y': 2 + 6,
    'A Z': 3 + 0,
    'B X': 1 + 0,
    'B Y': 2 + 3,
    'B Z': 3 + 6,
    'C X': 1 + 6,
    'C Y': 2 + 0,
    'C Z': 3 + 3,
}
with open('aoc02-input', 'r') as f:
    lines = f.readlines()
total_score = 0
for line in lines:
    line = line.strip()
    total_score += scoring[line]
print(total_score)
