#!/usr/bin/env python3
import curses
import itertools

"""
Rocks


####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
"""


def curses_init(stdscr):
    stdscr.clear()
    stdscr.refresh()
    curses.curs_set(0)  # make cursor invisible
    curses.start_color()
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)


def parse_instructions(filename):
    with open(filename) as f:
        return itertools.cycle(f.read().strip())


def draw(stdscr, falling_rock, rested_rocks):
    stdscr.clear()
    top_row = 50 # max(r for r, _ in falling_rock)
    for r in range(0, top_row + 1):
        for c in range(0, 9):
            if c in (0, 8):
                if r == 0:
                    stdscr.addstr(top_row - r, c, '+')  # corners
                else:
                    stdscr.addstr(top_row - r, c, '|')  # walls
            elif r == 0:
                stdscr.addstr(top_row - r, c, '-')      # floor
            else:
                stdscr.addstr(top_row - r, c, '.')      # empty space
    for r, c in falling_rock:
        stdscr.addstr(top_row - r, c, '@')              # falling rock
    for r, c in rested_rocks:
        stdscr.addstr(top_row - r, c, '#')              # resting rocks
    stdscr.refresh()


def hit_floor(rock):
    return min(r for r, _ in rock) <= 0


def hit_walls(rock):
    return max(c for _, c in rock) > 7 \
        or min(c for _, c in rock) < 1


def hit_resting_rocks(rock, rested_rocks):
    for rc in rock:
        if rc in rested_rocks:
            return True
    return False


def update(rock, instructions, rested_rocks):
    rested = False
    instruction = next(instructions)
    direction = 1 if instruction == '>' else -1
    blown_rock = tuple((r, c + direction) for r, c in rock)
    if not hit_walls(blown_rock) \
            and not hit_resting_rocks(blown_rock, rested_rocks):
        rock = blown_rock
    fallen_rock = tuple((r - 1, c) for r, c in rock)
    if hit_floor(fallen_rock) \
            or hit_resting_rocks(fallen_rock, rested_rocks):
        rested = True
    else:
        rock = fallen_rock
    return rock, rested


def new_rock(shape, row):
    if shape == '-':
        return (
            (row + 0, 3), (row + 0, 4), (row + 0, 5), (row + 0, 6))
    elif shape == '+':
        return (
                          (row + 2, 4),
            (row + 1, 3), (row + 1, 4), (row + 1, 5),
                          (row + 0, 4))
    elif shape == 'J':
        return (
                                        (row + 2, 5),
                                        (row + 1, 5),
            (row + 0, 3), (row + 0, 4), (row + 0, 5))
    elif shape == '|':
        return (
            (row + 3, 3),
            (row + 2, 3),
            (row + 1, 3),
            (row + 0, 3))
    elif shape == 'o':
        return (
            (row + 1, 3), (row + 1, 4),
            (row + 0, 3), (row + 0, 4))


def main(stdscr):
    curses_init(stdscr)
    instructions = parse_instructions('aoc17-input')
    shapes = itertools.cycle(('-', '+', 'J', '|', 'o'))
    rocks_count = 0
    top_row = 0
    falling_rock = new_rock(next(shapes), top_row + 4)
    rested_rocks = set()
    #draw(stdscr, falling_rock, rested_rocks)
    #stdscr.getch()
    while rocks_count < 2022:
        falling_rock, rested = update(falling_rock, instructions, rested_rocks)
        if rested:
            for r, c in falling_rock:
                top_row = max(top_row, r)
                rested_rocks.add((r, c))
            rocks_count += 1
            falling_rock = new_rock(next(shapes), top_row + 4)
        #draw(stdscr, falling_rock, rested_rocks)
        #stdscr.getch()
    stdscr.addstr(f'Rocks count: {rocks_count}\nTop row: {top_row}')
    stdscr.getch()


try:
    curses.wrapper(main)
except KeyboardInterrupt:
    pass
