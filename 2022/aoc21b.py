#!/usr/bin/env python3.11


def load_monkeys(filename):
    monkeys = {}
    for line in open(filename):
        monkey, eq = line.split(': ')
        eq = eq.split()
        if len(eq) == 1:
            monkeys[monkey] = float(eq[0])
        else:
            monkeys[monkey] = eq
    return monkeys


def calculate(v1, op, v2):
    if op == '+':
        return v1 + v2
    if op == '*':
        return v1 * v2
    if op == '-':
        return v1 - v2
    if op == '/':
        return v1 / v2


def reverse_op(op):
    if op == '+':
        return '-'
    if op == '*':
        return '/'
    if op == '-':
        return '+'
    if op == '/':
        return '*'



def solve_for(monkey, indent=0):
    eq = None
    if monkey in equals:
        eq = equals[monkey]
        del equals[monkey]
        del equals[eq]
        return solve_for(eq, indent=indent+2)
    if monkey in monkeys:
        eq = monkeys[monkey]
        if type(eq) is float:
            return eq
    else:
        for m in monkeys:
            if type(monkeys[m]) is not list \
                    or monkey not in (monkeys[m][0], monkeys[m][2]):
                continue
            v1 = monkeys[m][0]
            op = monkeys[m][1]
            v2 = monkeys[m][2]
            if monkey == v1:
                eq = [m, reverse_op(op), v2]
            elif monkey == v2:
                if op in ('+', '*'):
                    eq = [m, reverse_op(op), v1]
                else:
                    eq = [v1, op, m]
            del monkeys[m]
            break
    #print(('  ' * indent) + str(eq))
    return calculate(
    # return (  # to show the total equation
        solve_for(eq[0], indent=indent+2),
        eq[1],
        solve_for(eq[2], indent=indent+2))


monkeys = load_monkeys('aoc21-input')
equals = {
    monkeys['root'][0]: monkeys['root'][2],
    monkeys['root'][2]: monkeys['root'][0]}
del monkeys['humn']
del monkeys['root']
humn = solve_for('humn')
print(humn)
