#!/usr/bin/env python3


def is_hidden(grid, y, x):
    height = grid[y][x]
    northview_blocked = False
    southview_blocked = False
    westview_blocked = False
    eastview_blocked = False
    tmpy, tmpx = y, x
    while tmpy > 0:  # going north
        tmpy -= 1
        if grid[tmpy][tmpx] >= height:
            northview_blocked = True
            break
    tmpy = y
    while tmpy < (len(grid) - 1):  # going south
        tmpy += 1
        if grid[tmpy][tmpx] >= height:
            southview_blocked = True
            break
    tmpy = y
    while tmpx > 0:  # going west
        tmpx -= 1
        if grid[tmpy][tmpx] >= height:
            westview_blocked = True
            break
    tmpx = x
    while tmpx < (len(grid[0]) - 1):  # going east
        tmpx += 1
        if grid[tmpy][tmpx] >= height:
            eastview_blocked = True
            break
    return (
        northview_blocked and southview_blocked
        and eastview_blocked and westview_blocked)


grid = []
with open('aoc08-input', 'r') as f:
    for line in f.readlines():
        grid.append([int(c) for c in line.strip()])
visible_count = 0
for y in range(len(grid)):
    for x in range(len(grid[y])):
        if not is_hidden(grid, y, x):
            visible_count += 1
print(visible_count)
