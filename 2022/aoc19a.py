#!/usr/bin/env python3
from copy import copy


def parse_blueprints(filename):
    blueprints = {}
    for blueprint_description in open(filename):
        blueprint = {}
        blueprint_description = blueprint_description.strip()
        title, robot_descriptions = blueprint_description.split(': ')
        blueprint_id = int(title.split(' ')[1])
        for robot_description in robot_descriptions.split('.'):
            robot_description = robot_description.strip()
            if not robot_description:
                continue
            words = robot_description.replace(' and', '').split(' ')
            robot_type = words[1]
            words = words[4:]
            blueprint[robot_type] = {}
            while words:
                count = int(words.pop(0))
                resource = words.pop(0)
                blueprint[robot_type][resource] = count
        blueprints[blueprint_id] = blueprint
    return blueprints


def should_prune(robots, resources, minute, max_geodes):
    if not max_geodes:
        return False
    # if we build geode robot every minute
    # how much geodes can we get?
    geodes = resources['geode']     # current geodes count
    geode_robots = robots['geode']  # current geode robots count
    while minute < 25:
        geodes += geode_robots      # all geode robots mine
        geode_robots += 1           # and then we get next one
        minute += 1
    if geodes <= max_geodes:
        # this state cannot get any better than already found state
        return True
    return False


def find_max_geodes(blueprint):
    """ returns maximum number of geodes open in 24 min """
    max_geodes = 0
    robots = {'ore': 1, 'clay': 0, 'obsidian': 0, 'geode': 0}
    resources = {'ore': 0, 'clay': 0, 'obsidian': 0, 'geode': 0}
    init = (robots, resources, 1)
    q = [init]
    while q:
        robots, resources, minute = q.pop()
        # is this the last minute?
        if minute == 24:
            # no point in building new robots, let's just mine
            for resource, count in robots.items():
                resources[resource] += count
            if resources['geode'] > max_geodes:
                print(f'{robots} {resources}')
                max_geodes = resources['geode']
            continue
        if should_prune(robots, resources, minute, max_geodes):
            continue
        # figuring out what I can start building at the beginning of the minute
        affordable_robots = []
        for robot in blueprint:
            for resource in blueprint[robot]:
                if blueprint[robot][resource] > resources[resource]:
                    break
            else:
                affordable_robots.append(robot)
        # if I can afford all robots, there is no reason to save up anymore
        should_try_to_save_up = len(affordable_robots) < len(blueprint)
        # always build geode robot whenever possible and ignore other choices
        if 'geode' in affordable_robots:
            affordable_robots = ['geode']
            should_try_to_save_up = False
        elif minute == 23:
            # if we cannot build geode robot in 23rd minute, build nothing
            affordable_robots = []
            should_try_to_save_up = True
        # building robots
        for robot in affordable_robots:
            # don't build more robots than needed by highest price
            if robot != 'geode':
                max_req = max(
                    blueprint[rb][rs]
                    for rb in blueprint
                    for rs in blueprint[rb]
                    if rs == robot)
                if robots[robot] == max_req:
                    continue
            next_resources = copy(resources)
            # pay resources for the new robot
            for resource in blueprint[robot]:
                next_resources[resource] -= blueprint[robot][resource]
            # mine resources using the old robots
            for resource, count in robots.items():
                next_resources[resource] += count
            # robot is ready after minute's end
            next_robots = copy(robots)
            next_robots[robot] += 1
            if should_prune(
                    next_robots, next_resources, minute + 1, max_geodes):
                continue
            q.append((next_robots, next_resources, minute + 1))
        if should_try_to_save_up:
            # saving up = not building any new robot
            next_resources = copy(resources)
            for resource, count in robots.items():
                # mine resources using old robots
                next_resources[resource] += count
            if should_prune(robots, next_resources, minute + 1, max_geodes):
                continue
            q.append((robots, next_resources, minute + 1))
    return max_geodes


def main():
    blueprints = parse_blueprints('aoc19-input')
    sum_of_quality_levels = 0
    for blueprint_id in reversed(sorted(blueprints)):
        blueprint = blueprints[blueprint_id]
        print(f'Calculating blueprint {blueprint_id} ..')
        quality_level = blueprint_id * find_max_geodes(blueprint)
        print(f'  quality level: {quality_level}')
        sum_of_quality_levels += quality_level
    print(sum_of_quality_levels)


main()
