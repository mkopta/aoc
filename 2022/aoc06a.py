#!/usr/bin/env python3
with open('aoc06-input', 'r') as f:
    content = f.read().strip()
index = 4
marker = content[:4]
content = content[4:]
while content:
    if len(set(marker)) == 4:
        print(index)
        break
    marker = marker[1:] + content[:1]
    content = content[1:]
    index += 1
