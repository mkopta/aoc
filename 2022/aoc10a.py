#!/usr/bin/env python3
instructions = [line.strip() for line in open('aoc10-input').readlines()]
xhist = [None]
x = 1
for instruction in instructions:
    if instruction == 'noop':
        xhist.append(x)
    elif instruction.startswith('addx '):
        xhist.append(x)
        xhist.append(x)
        x += int(instruction[5:])
signal_strengths = []
for c in (20, 60, 100, 140, 180, 220):
    signal_strengths.append(xhist[c] * c)
print(sum(signal_strengths))
