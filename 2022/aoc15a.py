#!/usr/bin/env python3.11


def parse_number(word):
    return int(word.replace(':', '').replace(',', '').split('=')[1])
    

def parse_input(filename):
    sensor_to_beacon = {}
    for line in open(filename).readlines():
        words = line.split()
        sx, sy = parse_number(words[2]), parse_number(words[3])
        bx, by = parse_number(words[-2]), parse_number(words[-1])
        sensor_to_beacon[(sx, sy)] = (bx, by)
    return sensor_to_beacon


def manhattan_distance(xy1, xy2):
    x1, y1 = xy1
    x2, y2 = xy2
    return abs(x1 - x2) + abs(y1 - y2)


sensor_to_beacon = parse_input('aoc15-input')
ranges = []
Y = 2000000
#Y = 10
for sensor, beacon in sensor_to_beacon.items():
    dist = manhattan_distance(sensor, beacon)
    sx, sy = sensor
    if (sy - dist) <= Y and Y <= (sy + dist):
        n = abs(sy - Y)
        ranges.append([sx - dist + n, sx + dist - n])
ranges.sort()
unique_ranges = [ranges[0]]
while ranges:
    (x1, x2) = ranges.pop(0)
    if x1 > unique_ranges[-1][1]:
        unique_ranges.append((x1, x2))
        continue
    if x2 > unique_ranges[-1][1]:
        unique_ranges[-1][1] = x2
count = 0
for r in unique_ranges:
    count += abs(r[0] - r[1])
print(count)
