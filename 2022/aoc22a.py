#!/usr/bin/env python3.11
import re
import time

HEADINGS = ['>', 'v', '<', '^']


def parse_path_description(raw_path_description):
    raw_path_description = raw_path_description.strip()
    steps = re.findall(r'\d+', raw_path_description)
    turns = re.findall(r'\D+', raw_path_description)
    path_description = []
    while steps or turns:
        if steps:
            path_description.append(int(steps.pop(0)))
        if turns:
            path_description.append(turns.pop(0))
    return path_description


def parse_board(raw_board):
    open_tiles, wall_tiles, init = set(), set(), None
    row = 1
    for line in raw_board.split('\n'):
        col = 1
        for tile in line:
            if tile == '.':
                if not init:
                    init = (row, col)
                open_tiles.add((row, col))
            elif tile == '#':
                wall_tiles.add((row, col))
            col += 1
        row += 1
    return open_tiles, wall_tiles, init


def print_board(open_tiles, wall_tiles, current_position, heading):
    min_row, min_col = max_row, max_col = next(t for t in open_tiles)
    for tile in open_tiles | wall_tiles:
        row, col = tile
        min_row = min(min_row, row)
        min_col = min(min_col, col)
        max_row = max(max_row, row)
        max_col = max(max_col, col)
    for row in range(1, max_row + 1):
        for col in range(1, max_col + 1):
            tile = (row, col)
            if tile in open_tiles:
                if tile == current_position:
                    print(heading, end='')
                else:
                    print('.', end='')
            elif tile in wall_tiles:
                print('#', end='')
            else:
                print(' ', end='')
        print('')



def parse_input(filename):
    raw_board, raw_path_description = open(filename).read().split('\n\n')
    path_description = parse_path_description(raw_path_description)
    open_tiles, wall_tiles, current_position = parse_board(raw_board)
    return path_description, open_tiles, wall_tiles, current_position


def change_heading(heading, turn):
    idx = HEADINGS.index(heading)
    match turn:
        case 'R': idx = (idx + 1) % len(HEADINGS)
        case 'L': idx -= 1
    return HEADINGS[idx]


def increment_position(prev_position, heading):
    next_row, next_col = row, col = prev_position
    match heading:
        case '>': next_col += 1
        case 'v': next_row += 1
        case '<': next_col -= 1
        case '^': next_row -= 1
    next_position = next_row, next_col
    return next_position


def oposite_heading(heading):
    match heading:
        case '>': return '<'
        case '<': return '>'
        case '^': return 'v'
        case 'v': return '^'


def wrap_position(position, heading, open_tiles, wall_tiles):
    oheading = oposite_heading(heading)
    tiles = open_tiles | wall_tiles
    while position in tiles:
        prev = position
        position = increment_position(position, oheading)
    return prev


def move_by_one_step(prev_position, heading, open_tiles, wall_tiles):
    next_position = increment_position(prev_position, heading)
    if next_position in open_tiles:
        # we can move there
        return next_position
    elif next_position in wall_tiles:
        # we are stuck behind a wall
        return prev_position
    else:
        # we are out-of-bounds, have to wrap around check if we can go there
        next_position = wrap_position(
            prev_position, heading, open_tiles, wall_tiles)
        if next_position in open_tiles:
            return next_position
        else:  # if next_position in wall_tiles
            return prev_position


path_description, open_tiles, wall_tiles, current_position \
    = parse_input('aoc22-input')
heading = '>'
#print_board(open_tiles, wall_tiles, current_position, heading)
#print(path_description)
for steps in path_description:
    if steps in ('L', 'R'):
        heading = change_heading(heading, steps)
    else:
        for _ in range(steps):
            current_position = move_by_one_step(
                current_position, heading, open_tiles, wall_tiles)
row, col = current_position
print(1000 * row + 4 * col + HEADINGS.index(heading))
