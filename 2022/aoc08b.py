#!/usr/bin/env python3


def calc_scenic_score(grid, y, x):
    height = grid[y][x]
    north_score = 0
    south_score = 0
    west_score = 0
    east_score = 0
    tmpy, tmpx = y, x
    while tmpy > 0:  # going north
        tmpy -= 1
        north_score += 1
        if grid[tmpy][tmpx] >= height:
            break
    tmpy = y
    while tmpy < (len(grid) - 1):  # going south
        tmpy += 1
        south_score += 1
        if grid[tmpy][tmpx] >= height:
            break
    tmpy = y
    while tmpx > 0:  # going west
        tmpx -= 1
        west_score += 1
        if grid[tmpy][tmpx] >= height:
            break
    tmpx = x
    while tmpx < (len(grid[0]) - 1):  # going east
        tmpx += 1
        east_score += 1
        if grid[tmpy][tmpx] >= height:
            break
    return north_score * south_score * west_score * east_score


grid = []
with open('aoc08-input', 'r') as f:
    for line in f.readlines():
        grid.append([int(c) for c in line.strip()])
max_scenic_score = 0
for y in range(len(grid)):
    for x in range(len(grid[y])):
        max_scenic_score = max(max_scenic_score, calc_scenic_score(grid, y, x))
print(max_scenic_score)
