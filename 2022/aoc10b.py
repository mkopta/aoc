#!/usr/bin/env python3
instructions = [line.strip() for line in open('aoc10-input').readlines()]
xhist = [None]
x = 1
for instruction in instructions:
    if instruction == 'noop':
        xhist.append(x)
    elif instruction.startswith('addx '):
        xhist.append(x)
        xhist.append(x)
        x += int(instruction[5:])
for c in range(1, 241):
    pos = (c - 1) % 40
    print(
        '#' if pos in (xhist[c] - 1, xhist[c], xhist[c] + 1) else '.',
        end='' if c % 40 != 0 else '\n')
