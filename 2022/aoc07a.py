#!/usr/bin/env python3

class Dir:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.content = []
        self.size = 0
    def __repr__(self):
        return f'{self.name}/' if self.name != '/' else '/'


class File:
    def __init__(self, name, size):
        self.name = name
        self.size = size
    def __repr__(self):
        return f'{self.name}'


def read_tree():
    lines = []
    with open('aoc07-input', 'r') as f:
        for line in f.readlines():
            lines.append(line.strip())
    current_dir = root = Dir('/', None)
    assert lines[0] == '$ cd /'
    i = 1
    while i < len(lines):
        assert lines[i].startswith('$ ')
        line = lines[i][2:]
        if line.startswith('cd '):
            i += 1
            target = line.split()[1]
            if target == '..':
                current_dir = current_dir.parent
            else:
                for x in current_dir.content:
                    if type(x) is Dir and x.name == target:
                        current_dir = x
                        break
        elif line == 'ls':
            i += 1
            content = []
            while i < len(lines) and not lines[i].startswith('$ '):
                x, name = lines[i].split()
                if x == 'dir':
                    content.append(Dir(name, current_dir))
                else:
                    content.append(File(name, int(x)))
                i += 1
            current_dir.content = content
    return root


def calc_totals(node):
    if type(node) is File:
        return node.size
    total = 0
    for c in node.content:
        total += calc_totals(c)
    node.size = total
    return total


def print_tree(node, indent=0):
    print(indent * ' ' + f'{node} ({node.size})')
    indent += 2
    if type(node) is Dir:
        for c in node.content:
            print_tree(c, indent)


def sum_of_dirs_with_total_at_most_100000(node):
    if type(node) is File:
        return 0
    total = node.size if node.size <= 100000 else 0
    for c in node.content:
        total += sum_of_dirs_with_total_at_most_100000(c)
    return total


root = read_tree()
calc_totals(root)
#print_tree(root)
print(sum_of_dirs_with_total_at_most_100000(root))
