#!/usr/bin/env python3.11

min_x, min_y, min_z = None, None, None
max_x, max_y, max_z = None, None, None
droplet_cubes = {}

for line in open('aoc18-input'):
    x, y, z = list(map(int, line.split(',')))
    droplet_cubes[(x, y, z)] = 0
    min_x = min(min_x, x) if min_x is not None else x
    min_y = min(min_y, y) if min_y is not None else y
    min_z = min(min_z, z) if min_z is not None else z
    max_x = max(max_x, x) if max_x is not None else x
    max_y = max(max_y, y) if max_y is not None else y
    max_z = max(max_z, z) if max_z is not None else z

# expand the bounding box +1 in each direction
min_xyz = min_x - 1, min_y - 1, min_z - 1
max_xyz = max_x + 1, max_y + 1, max_z + 1

def expand(xyz):
    x, y, z = xyz
    return (
        (x + 1, y    , z    ),
        (x    , y + 1, z    ),
        (x    , y    , z + 1),
        (x - 1, y    , z    ),
        (x    , y - 1, z    ),
        (x    , y    , z - 1))

def is_oob(xyz, min_xyz, max_xyz):
    x, y, z = xyz
    min_x, min_y, min_z = min_xyz
    max_x, max_y, max_z = max_xyz
    if ((min_x <= x <= max_x)
            and (min_y <= y <= max_y)
            and (min_z <= z <= max_z)):
        return False
    return True

visited_states = set()
open_states = [min_xyz]
while open_states:
    s = open_states.pop(0)
    for e in expand(s):
        if is_oob(e, min_xyz, max_xyz):
            continue
        if e in droplet_cubes:
            droplet_cubes[e] += 1
            continue
        if e not in visited_states and e not in open_states:
            open_states.append(e)
    visited_states.add(s)

print(sum(s for s in droplet_cubes.values()))
