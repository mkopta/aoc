#!/usr/bin/env python3.11


def load_elves(filename):
    elves = set()
    row, col = 1, 1
    for line in open(filename):
        for character in line:
            if character == '#':
                elves.add((row, col))
            col += 1
        row += 1
        col = 1
    return elves


def determine_bounding_box(elves):
    min_row, min_col = max_row, max_col = next(e for e in elves)
    for row, col in elves:
        min_row = min(min_row, row)
        max_row = max(max_row, row)
        min_col = min(min_col, col)
        max_col = max(max_col, col)
    return min_row, max_row, min_col, max_col


def try_to_move(direction, elf, elves):
    r, c = elf
    match direction:
        case 'N':
            x1, x2, x3 = [(r - 1, c + o) for o in (-1, 0, 1)]
        case 'S':
            x1, x2, x3 = [(r + 1, c + o) for o in (-1, 0, 1)]
        case 'E':
            x1, x2, x3 = [(r + o, c + 1) for o in (-1, 0, 1)]
        case 'W':
            x1, x2, x3 = [(r + o, c - 1) for o in (-1, 0, 1)]
    if x1 not in elves and x2 not in elves and x3 not in elves:
        return x2
    return None


def are_elves_around(elf, elves):
    row, col = elf
    for r in (row - 1, row, row + 1):
        for c in (col - 1, col, col + 1):
            if (r, c) == elf:
                continue
            if (r, c) in elves:
                return True
    return False


directions = ['N', 'S', 'W', 'E']
elves = load_elves('aoc23-input')
round_number = 0
while True:
    round_number += 1
    elves_moved = False
    proposals = {}
    not_moved = set()
    for elf in elves:
        if not are_elves_around(elf, elves):
            not_moved.add(elf)
            continue
        for direction in directions:
            if new_pos := try_to_move(direction, elf, elves):
                if new_pos in proposals:
                    proposals[new_pos].append(elf)
                else:
                    proposals[new_pos] = [elf]
                break
        else:
            not_moved.add(elf)
    elves = not_moved
    for new_pos, proposing_elves in proposals.items():
        if len(proposing_elves) == 1:
            elves.add(new_pos)
            elves_moved = True
        else:
            for elf in proposing_elves:
                elves.add(elf)
    directions = directions[1:] + [directions[0]]
    if not elves_moved:
        print(round_number)
        break
