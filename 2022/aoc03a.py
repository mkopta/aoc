#!/usr/bin/env python3
alphabet = 'abcdefghijklmnopqrstuvwxyz'
total = 0
with open('aoc03-input', 'r') as f:
    for line in f.readlines():
        line = line.strip()
        idx = len(line) // 2
        common_items = set(line[:idx]) & set(line[idx:])
        for item in common_items:
            if item in alphabet:
                total += alphabet.index(item) + 1
            else:
                total += alphabet.upper().index(item) + 27
print(total)
