#!/usr/bin/env python3


def parse_monkeys(content):
    monkeys = []
    for section in content.split('\n\n'):
        lines = section.split('\n')
        monkey = {}
        monkey['number'] = int(lines[0].replace(':', '').split()[1])
        monkey['items'] = list(map(int, lines[1].split(': ')[1].split(', ')))
        monkey['operation'] = lines[2].split('= ')[1]
        monkey['test'] = {
            'divisible by': int(lines[3].split()[-1]),
            'pass': int(lines[4].split()[-1]),
            'fail': int(lines[5].split()[-1])}
        monkey['inspected_items_count'] = 0
        monkeys.append(monkey)
    return monkeys


def do_operation(op, item):
    return eval(op.replace('old', str(item)))


def throw_item_to(item, monkey_number, monkeys):
    for monkey in monkeys:
        if monkey['number'] == monkey_number:
            monkey['items'].append(item)
            return


def do_round(monkeys, magic_number):
    for monkey in monkeys:
        for item in monkey['items']:
            item = do_operation(monkey['operation'], item)
            item = item % magic_number
            if item % monkey['test']['divisible by'] == 0:
                throw_item_to(item, monkey['test']['pass'], monkeys)
            else:
                throw_item_to(item, monkey['test']['fail'], monkeys)
            monkey['inspected_items_count'] += 1
        monkey['items'] = []


def print_inspection_count(monkey):
    print(
        f'Monkey {monkey["number"]} inspected items'
        f' {monkey["inspected_items_count"]} times.')


monkeys = parse_monkeys(open('aoc11-input').read())

magic_number = 1
for monkey in monkeys:
    magic_number *= monkey['test']['divisible by']

for i in range(1, 10001):
    do_round(monkeys, magic_number)
    if i % 10 == 0:
        print(f'== After round {i} ==')
        for monkey in monkeys:
            print_inspection_count(monkey)
        print()

activity = sorted(
    (m['inspected_items_count'] for m in monkeys), reverse=True)

print(f'\nMonkey business: {activity[0] * activity[1]}')
