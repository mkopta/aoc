#!/usr/bin/env python3.11


def load_monkeys(filename):
    monkeys = {}
    for line in open(filename):
        monkey, eq = line.split(': ')
        eq = eq.split()
        if len(eq) == 1:
            monkeys[monkey] = int(eq[0])
        else:
            monkeys[monkey] = eq
    return monkeys


def calculate(v1, op, v2):
    if op == '+':
        return v1 + v2
    if op == '*':
        return v1 * v2
    if op == '-':
        return v1 - v2
    if op == '/':
        return v1 / v2


def value_of(monkey, monkeys):
    v = monkeys[monkey]
    if type(v) is int:
        return v
    return calculate(value_of(v[0], monkeys), v[1], value_of(v[2], monkeys))


monkeys = load_monkeys('aoc21-input')
print(value_of('root', monkeys))
