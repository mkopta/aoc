#!/usr/bin/env python3


def expand(xy, area):
    x, y = xy
    h = ord(area[y][x] if area[y][x] != 'S' else 'a')
    exys = []
    if y < len(area) - 1 and ord(area[y+1][x]) <= h+1:
        exys.append((x, y+1))
    if y > 0 and ord(area[y-1][x]) <= h+1:
        exys.append((x, y-1))
    if x < len(area[0])-1 and ord(area[y][x+1]) <= h+1:
        exys.append((x+1, y))
    if x > 0 and ord(area[y][x-1]) <= h+1:
        exys.append((x-1, y))
    return exys



def walk(area, S, E):
    queue = [S]
    visited = set()
    parents = {S: None}
    while queue:
        xy = queue.pop(0)
        visited.add(xy)
        x, y = xy
        if xy == E:
            break
        for exy in expand(xy, area):
            if exy not in visited and exy not in queue:
                queue.append(exy)
                parents[exy] = xy
    else:
        return None
    xy = E
    steps = 0
    while xy:
        x, y = xy
        xy = parents[xy]
        steps += 1
    return steps - 1


def main():
    area = []
    for line in open('aoc12-input').read().strip().split():
        area.append([c for c in line])
    for y in range(len(area)):
        for x in range(len(area[y])):
            if area[y][x] == 'S':
                S = (x, y)
            elif area[y][x] == 'E':
                E = (x, y)
    starts = [
        (x, y)
        for y in range(len(area))
        for x in range(len(area[y]))
        if area[y][x] in ('a', 'S')]
    lenghts = []
    for s in starts:
        steps = walk(area, s, E)
        if steps is not None:
            lenghts.append(steps)
    print(min(lenghts))


main()
