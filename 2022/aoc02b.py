#!/usr/bin/env python3
# Rock (A), Paper (B), Scissors (C)
# Lose (X), Draw (Y), Win (Z)
scoring = {
    'A X': 3 + 0,
    'A Y': 1 + 3,
    'A Z': 2 + 6,

    'B X': 1 + 0,
    'B Y': 2 + 3,
    'B Z': 3 + 6,

    'C X': 2 + 0,
    'C Y': 3 + 3,
    'C Z': 1 + 6,
}
with open('aoc02-input', 'r') as f:
    lines = f.readlines()
total_score = 0
for line in lines:
    line = line.strip()
    total_score += scoring[line]
print(total_score)
