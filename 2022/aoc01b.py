#!/usr/bin/env python3

with open('aoc01-input', 'r') as f:
    lines = f.readlines()

numbers = []
for line in lines:
    l = line.rstrip()
    if l:
        numbers.append(int(l))
    else:
        numbers.append(None)
numbers.append(None)

elves = []
elf = []
for number in numbers:
    if number is not None:
        elf.append(number)
    else:
        elves.append(elf)
        elf = []

totals = []
for elf in elves:
    total_calories = sum(elf)
    totals.append(total_calories)
print(sum(sorted(totals, reverse=True)[:3]))
