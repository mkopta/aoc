#!/usr/bin/env python3.11
cubes = []
for line in open('aoc18-input'):
    cube = list(map(int, line.split(','))) + [6]
    cubes.append(cube)


def is_touching(cube1, cube2):
    x1, y1, z1, _ = cube1
    x2, y2, z2, _ = cube2
    distx, disty, distz = abs(x1 - x2), abs(y1 - y2), abs(z1 - z2)
    if (distx, disty, distz) in ((1, 0, 0), (0, 1, 0), (0, 0, 1)):
        return True
    return False


for i in range(len(cubes)):
    for j in range(i + 1, len(cubes)):
        if is_touching(cubes[i], cubes[j]):
            cubes[i][-1] -= 1
            cubes[j][-1] -= 1
            
print(sum(cube[-1] for cube in cubes))
