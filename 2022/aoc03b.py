#!/usr/bin/env python3
alphabet = 'abcdefghijklmnopqrstuvwxyz'
total = 0
with open('aoc03-input', 'r') as f:
    lines = f.readlines()
while lines:
    elf1 = lines.pop(0).strip()
    elf2 = lines.pop(0).strip()
    elf3 = lines.pop(0).strip()
    badge = set(elf1) & set(elf2) & set(elf3)
    assert len(badge) == 1
    badge = badge.pop()
    if badge in alphabet:
        total += alphabet.index(badge) + 1
    else:
        total += alphabet.upper().index(badge) + 27
print(total)
