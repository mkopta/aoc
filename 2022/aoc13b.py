#!/usr/bin/env python3
from copy import deepcopy
from functools import cmp_to_key

def compare(l, r):
    l, r = deepcopy(l), deepcopy(r)
    if type(l) is list and type(r) is list:
        while True:
            if not l and not r:
                return 0
            if not l and r:
                return -1
            if l and not r:
                return 1
            lv = l.pop(0)
            rv = r.pop(0)
            if rv := compare(lv, rv):
                return rv
    elif type(l) is int and type(r) is int:
        if l > r:
            return 1
        elif l < r:
            return -1
        else:
            return 0
    else:  # mixed types
        if type(l) is int:
            return compare([l], r)
        else:
            return compare(l, [r])


packets = []
for block in open('aoc13-input').read().strip().split('\n\n'):
    left, right = map(eval, block.split('\n'))
    packets.append(left)
    packets.append(right)
divider1 = [[2]]
divider2 = [[6]]
packets.extend([divider1, divider2])
packets.sort(key=cmp_to_key(compare))
print((packets.index(divider1) + 1) * (packets.index(divider2) + 1))
