#!/usr/bin/env python3

class Dir:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.content = []
        self.size = 0
    def __repr__(self):
        return f'{self.name}/' if self.name != '/' else '/'


class File:
    def __init__(self, name, size):
        self.name = name
        self.size = size
    def __repr__(self):
        return f'{self.name}'


def read_tree():
    lines = []
    with open('aoc07-input', 'r') as f:
        for line in f.readlines():
            lines.append(line.strip())
    current_dir = root = Dir('/', None)
    assert lines[0] == '$ cd /'
    i = 1
    while i < len(lines):
        assert lines[i].startswith('$ ')
        line = lines[i][2:]
        if line.startswith('cd '):
            i += 1
            target = line.split()[1]
            if target == '..':
                current_dir = current_dir.parent
            else:
                for x in current_dir.content:
                    if type(x) is Dir and x.name == target:
                        current_dir = x
                        break
        elif line == 'ls':
            i += 1
            content = []
            while i < len(lines) and not lines[i].startswith('$ '):
                x, name = lines[i].split()
                if x == 'dir':
                    content.append(Dir(name, current_dir))
                else:
                    content.append(File(name, int(x)))
                i += 1
            current_dir.content = content
    return root


def calc_totals(node):
    if type(node) is File:
        return node.size
    total = 0
    for c in node.content:
        total += calc_totals(c)
    node.size = total
    return total


def find_dirs(node):
    dirs = []
    for c in node.content:
        if type(c) is File:
            continue
        dirs.append(c)
        dirs.extend(find_dirs(c))
    return dirs


root = read_tree()
calc_totals(root)
dirs = find_dirs(root)
total_disk_space = 70_000_000
space_needed_by_update = 30_000_000
free_space = total_disk_space - root.size
need_to_free_for_update = space_needed_by_update - free_space
print(sorted(d.size for d in dirs if d.size >= need_to_free_for_update)[0])
