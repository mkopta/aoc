#!/usr/bin/env python3
with open('aoc05-input', 'r') as f:
    content = f.read()
raw_stacks, raw_moves = content.split('\n\n')
last_stack_number = int(raw_stacks.split('\n')[-1].split()[-1])
assert last_stack_number < 10
stacks = {n: [] for n in range(1, last_stack_number + 1)}
# stacks parsing (could've done this manually..)
for line in raw_stacks.split('\n')[:-1]:
    idx = -1
    for _ in range(line.count('[')):
        idx = line.index('[', idx + 1)
        stack_number = idx // 4 + 1
        stacks[stack_number].insert(0, line[idx+1])
for raw_move in raw_moves.split('\n'):
    if not raw_move:
        break
    instructions = raw_move.split()
    count = int(instructions[1])
    from_stack = int(instructions[3])
    to_stack = int(instructions[5])
    for _ in range(count):
        stacks[to_stack].append(stacks[from_stack].pop())
print(''.join(stacks[i][-1] for i in range(1, last_stack_number + 1)))
