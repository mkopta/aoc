#!/usr/bin/env python3.11
import itertools


def parse_structures(filename):
    structures = []
    with open(filename) as f:
        for line in f.readlines():
            coords = line.strip().split(' -> ')
            structure = []
            for coord1, coord2 in itertools.pairwise(coords):
                edge = [
                    list(map(int, coord1.split(','))),
                    list(map(int, coord2.split(',')))]
                structure.append(edge)
            structures.append(structure)
    return structures


def normalize_x(structures, padding=0):
    min_x, max_x = None, None
    min_y, max_y = None, None
    # Find the ranges of x and y
    for structure in structures:
        for edge in structure:
            for point in edge:
                x, y = point
                min_x = min(min_x, x) if min_x is not None else x
                max_x = max(max_x, x) if max_x is not None else x
                min_y = min(min_y, y) if min_y is not None else y
                max_y = max(max_y, y) if max_y is not None else y
    assert min_x >= padding
    xshift = min_x - padding
    # Normalize all x coordinates
    for structure in structures:
        for edge in structure:
            for point in edge:
                point[0] -= xshift
    # Return width, height and normalization shift
    return (max_x - xshift) + 1, max_y + 1, xshift


def interpolate(p1, p2):
    points = [p1]
    px, py = p1
    p2x, p2y = p2
    while px < p2x:
        px += 1
        points.append((px, py))
    while py < p2y:
        py += 1
        points.append((px, py))
    while px > p2x:
        px -= 1
        points.append((px, py))
    while py > p2y:
        py -= 1
        points.append((px, py))
    points.append(p2)
    return points


def create_area(structures, width, height):
    area = []
    for y in range(height):
        layer = []
        for x in range(width):
            layer = ['.'] * width
        area.append(layer)
    for structure in structures:
        for edge in structure:
            p1, p2 = edge
            points = interpolate(p1, p2)
            for (x, y) in points:
                area[y][x] = '#'
    return area


def pour_sand(area, width, height, xshift):
    sx, sy = (500 - xshift, 0)
    while True:
        if sx == 0 or sx == width - 1 or sy == height - 1:
            return None  # out of bounds
        if area[sy+1][sx] == '.':
            sy += 1
        elif area[sy+1][sx-1] == '.':
            sx -= 1
            sy += 1
        elif area[sy+1][sx+1] == '.':
            sx += 1
            sy += 1
        else:
            return sx, sy


def main():
    structures = parse_structures('aoc14-input')
    # enough padding added to both sides
    padding = 300  # meh..
    width, height, xshift = normalize_x(structures, padding=padding)
    width += padding
    height += 2
    # add ground
    structures.append([[[0, height-1], [width-1, height-1]]])
    area = create_area(structures, width, height)
    rested_sand = 0
    while True:
        sand = pour_sand(area, width, height, xshift)
        if not sand:
            print('Out of bounds!\n')
            break
        rested_sand += 1
        sx, sy = sand
        if sy == 0:
            break
        area[sy][sx] = 'o'
    print(f'{rested_sand=}')

try:
    main()
except KeyboardInterrupt:
    pass
