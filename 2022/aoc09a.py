#!/usr/bin/env python3
#
#            y = -1
#   x = -1  x,y=  0  x =  1
#            y =  1
#
class Point:
    def __init__(self):
        self.x = 0
        self.y = 0

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f'x={self.x} y={self.y}'

    def is_adjacent(self, other):
        return abs(self.x - other.x) < 2 and abs(self.y - other.y) < 2

    def move_towards(self, other):
        if self.x < other.x:
            self.x += 1
        if self.y < other.y:
            self.y += 1
        if self.x > other.x:
            self.x -= 1
        if self.y > other.y:
            self.y -= 1


head, tail = Point(), Point()
tail_visited_xys = {(tail.x, tail.y)}
with open('aoc09-input', 'r') as f:
    for instruction in f.readlines():
        direction, count = instruction.split()
        count = int(count)
        for _ in range(count):
            if direction == 'U':
                head.y -= 1
            elif direction == 'D':
                head.y += 1
            elif direction == 'L':
                head.x -= 1
            elif direction == 'R':
                head.x += 1
            if head == tail or tail.is_adjacent(head):
                continue
            tail.move_towards(head)
            tail_visited_xys.add((tail.x, tail.y))
print(len(tail_visited_xys))
