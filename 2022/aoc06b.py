#!/usr/bin/env python3
with open('aoc06-input', 'r') as f:
    content = f.read().strip()
index = 14
marker = content[:14]
content = content[14:]
while content:
    if len(set(marker)) == 14:
        print(index)
        break
    marker = marker[1:] + content[:1]
    content = content[1:]
    index += 1
