#!/usr/bin/env python3
import math
import functools
from copy import copy


def parse_blueprints(filename):
    blueprints = {}
    for blueprint_description in open(filename):
        blueprint = {}
        blueprint_description = blueprint_description.strip()
        title, robot_descriptions = blueprint_description.split(': ')
        blueprint_id = int(title.split(' ')[1])
        for robot_description in robot_descriptions.split('.'):
            robot_description = robot_description.strip()
            if not robot_description:
                continue
            words = robot_description.replace(' and', '').split(' ')
            robot_type = words[1]
            words = words[4:]
            blueprint[robot_type] = {}
            while words:
                count = int(words.pop(0))
                resource = words.pop(0)
                blueprint[robot_type][resource] = count
        blueprints[blueprint_id] = blueprint
    return blueprints


def should_prune(robots, resources, minute, max_geodes, limit):
    if not max_geodes:
        return False
    # if we build geode robot every minute
    # how much geodes can we get?
    geodes = resources['geode']     # current geodes count
    geode_robots = robots['geode']  # current geode robots count
    while minute <= limit:
        geodes += geode_robots      # all geode robots mine
        geode_robots += 1           # and then we get next one
        minute += 1
    if geodes <= max_geodes:
        # this state cannot get any better than already found state
        return True
    return False


def find_max_geodes(blueprint, limit=32):
    """ returns maximum number of geodes open in {limit} min """
    max_geodes = 0
    # precalculate highest robot cost for any resource
    highest_requirements = {
        resource: max(
            blueprint[rb][rs]
            for rb in blueprint
            for rs in blueprint[rb]
            if rs == resource)
        for resource in ('ore', 'clay', 'obsidian')}
    highest_requirements['geode'] = 1000
    # create initial state
    robots = {'ore': 1, 'clay': 0, 'obsidian': 0, 'geode': 0}
    resources = {'ore': 0, 'clay': 0, 'obsidian': 0, 'geode': 0}
    q = [(robots, resources, 1)]
    while q:
        robots, resources, minute = q.pop()
        if minute == limit:
            for resource, count in robots.items():
                resources[resource] += count
            if resources['geode'] > max_geodes:
                #print(f'{robots} {resources}')
                max_geodes = resources['geode']
            continue
        if should_prune(robots, resources, minute, max_geodes, limit):
            continue
        choices = False
        for robot, build_until in (
                ('geode', limit),
                ('obsidian', limit - 1),
                ('clay', limit - 1),
                ('ore', limit - 3)):
            if robots[robot] < highest_requirements[robot] \
                    and all(
                        robots[resource] > 0
                        for resource in blueprint[robot]):
                minutes_required_to_get_resources = max(
                    [0] + [
                        math.ceil(
                            (
                                blueprint[robot][resource]
                                - resources[resource]
                            ) / robots[resource])
                        for resource in blueprint[robot]])
                next_robots = copy(robots)
                next_resources = copy(resources)
                time_jump = minutes_required_to_get_resources + 1
                next_minute = minute + time_jump
                if next_minute <= build_until:
                    for resource in resources:
                        next_resources[resource] += \
                            robots[resource] * time_jump
                    for resource in blueprint[robot]:
                        next_resources[resource] -= blueprint[robot][resource]
                    next_robots[robot] += 1
                    q.append((next_robots, next_resources, next_minute))
                    choices = True
        if not choices:
            # jump to the end
            minutes_remaining = limit - minute
            for resource in resources:
                resources[resource] += robots[resource] * minutes_remaining
            q.append((robots, resources, minute + minutes_remaining))
    return max_geodes


def main():
    blueprints = parse_blueprints('aoc19-input')
    max_geodes = {}
    for blueprint_id in sorted(blueprints):
        if blueprint_id not in (1, 2, 3):
            continue
        blueprint = blueprints[blueprint_id]
        print(f'Calculating blueprint {blueprint_id} .. ')
        max_geodes[blueprint_id] = find_max_geodes(blueprint)
        print(f'  max geodes: {max_geodes[blueprint_id]}')
    print(functools.reduce(lambda a, b: a * b, max_geodes.values()))


main()
