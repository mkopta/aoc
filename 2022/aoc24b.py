#!/usr/bin/env python3.11
import time
from collections import defaultdict


def load(filename):
    blizzards = defaultdict(list)
    start, end = None, None
    for r, line in enumerate(open(filename), 1):
        for c, char in enumerate(line[:-1], 1):
            match char:
                case '.':
                    if not start:
                        start = (r, c)
                    end = (r, c)
                case '#': continue
                case blizzard:
                    blizzards[(r, c)].append(blizzard)
    return (r, c), blizzards, start, end


def move(blizzard, r, c):
    match blizzard:
        case '>': return r    , c + 1
        case '<': return r    , c - 1
        case '^': return r - 1, c
        case 'v': return r + 1, c


def update(size, blizzards):
    updated_blizzards = defaultdict(list)
    for (r, c) in blizzards:
        for blizzard in blizzards[(r, c)]:
            ur, uc = move(blizzard, r, c)
            if ur == size[0]:
                ur = 2
            if uc == size[1]:
                uc = 2
            if ur == 1:
                ur = size[0] - 1
            if uc == 1:
                uc = size[1] - 1
            updated_blizzards[(ur, uc)].append(blizzard)
    return updated_blizzards


def navigate(start, end, blizzards, size):
    states = {start}
    minute = 0
    while True:
        minute += 1
        blizzards = update(size, blizzards)
        new_states = set()
        for (r, c) in states:
            if not blizzards[(r, c)]:
                new_states.add((r, c))
            for (er, ec) in ((r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)):
                if (er, ec) == end:
                    return minute, blizzards
                if blizzards[(er, ec)]:
                    continue
                if er in (1, size[0]) or ec in (1, size[1]):
                    continue
                new_states.add((er, ec))
        states = new_states


def main():
    size, blizzards, start, end = load('aoc24-input')
    total_minutes = 0
    minutes, blizzards = navigate(start, end, blizzards, size)
    print(minutes)
    total_minutes += minutes
    minutes, blizzards = navigate(end, start, blizzards, size)
    print(minutes)
    total_minutes += minutes
    minutes, blizzards = navigate(start, end, blizzards, size)
    print(minutes)
    total_minutes += minutes
    print(total_minutes)


try:
    main()
except KeyboardInterrupt:
    pass
