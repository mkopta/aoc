#!/usr/bin/env python3.11
import time
import curses
import itertools

VISUALIZE = True


def curses_init(stdscr):
    stdscr.clear()
    stdscr.refresh()
    curses.curs_set(0)  # make cursor invisible
    curses.start_color()
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)


def parse_structures(filename):
    structures = []
    with open(filename) as f:
        for line in f.readlines():
            coords = line.strip().split(' -> ')
            structure = []
            for coord1, coord2 in itertools.pairwise(coords):
                edge = [
                    list(map(int, coord1.split(','))),
                    list(map(int, coord2.split(',')))]
                structure.append(edge)
            structures.append(structure)
    return structures


def normalize_x(structures):
    """ Remap of x to 0. """
    min_x, max_x = None, None
    min_y, max_y = None, None
    # Find the ranges of x and y
    for structure in structures:
        for edge in structure:
            for point in edge:
                x, y = point
                min_x = min(min_x, x) if min_x is not None else x
                max_x = max(max_x, x) if max_x is not None else x
                min_y = min(min_y, y) if min_y is not None else y
                max_y = max(max_y, y) if max_y is not None else y
    # Normalize all x coordinates
    for structure in structures:
        for edge in structure:
            for point in edge:
                point[0] -= min_x
    # Return width, height and normalization shift
    return (max_x - min_x) + 1, max_y + 1, min_x


def interpolate(p1, p2):
    points = [p1]
    px, py = p1
    p2x, p2y = p2
    while px < p2x:
        px += 1
        points.append((px, py))
    while py < p2y:
        py += 1
        points.append((px, py))
    while px > p2x:
        px -= 1
        points.append((px, py))
    while py > p2y:
        py -= 1
        points.append((px, py))
    points.append(p2)
    return points


def create_area(structures, width, height):
    area = []
    for y in range(height):
        layer = []
        for x in range(width):
            layer = ['.'] * width
        area.append(layer)
    for structure in structures:
        for edge in structure:
            p1, p2 = edge
            points = interpolate(p1, p2)
            for (x, y) in points:
                area[y][x] = '#'
    return area


def draw_area(area, stdscr):
    for y in range(len(area)):
        for x in range(len(area[y])):
            if area[y][x] == '.':
                stdscr.addstr(y, x, area[y][x], curses.A_DIM)
            else:
                stdscr.addstr(y, x, area[y][x])
    stdscr.refresh()


def pour_sand(area, width, height, xshift, stdscr):
    sx, sy = (500 - xshift, 0)
    while True:
        if VISUALIZE:
            time.sleep(0.1)
            draw_area(area, stdscr)
            stdscr.addstr(sy, sx, 'o')
            stdscr.refresh()
        if sx == 0 or sx == width - 1 or sy == height - 1:
            return None  # out of bounds
        if area[sy+1][sx] == '.':
            sy += 1
        elif area[sy+1][sx-1] == '.':
            sx -= 1
            sy += 1
        elif area[sy+1][sx+1] == '.':
            sx += 1
            sy += 1
        else:
            return sx, sy


def main(stdscr):
    curses_init(stdscr)
    structures = parse_structures('aoc14-example')
    width, height, xshift = normalize_x(structures)
    area = create_area(structures, width, height)
    if VISUALIZE:
        draw_area(area, stdscr)
    rested_sand = 0
    while True:
        sand = pour_sand(area, width, height, xshift, stdscr)
        if not sand:
            break
        rested_sand += 1
        sx, sy = sand
        area[sy][sx] = 'o'
    if VISUALIZE:
        stdscr.addstr(height + 2, 0, f'{rested_sand=}')
    else:
        stdscr.addstr(f'{rested_sand=}')
    stdscr.getch()

try:
    curses.wrapper(main)
except KeyboardInterrupt:
    pass
