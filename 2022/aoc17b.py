#!/usr/bin/env python3
import curses
import itertools


def parse_instructions(filename):
    with open(filename) as f:
        return itertools.cycle(enumerate(f.read().strip()))


def hit_floor(rock):
    return min(r for r, _ in rock) <= 0


def hit_walls(rock):
    return max(c for _, c in rock) > 7 \
        or min(c for _, c in rock) < 1


def hit_resting_rocks(rock, rested_rocks):
    for rc in rock:
        if rc in rested_rocks:
            return True
    return False


def update(rock, instruction, rested_rocks):
    rested = False
    direction = 1 if instruction == '>' else -1
    blown_rock = tuple((r, c + direction) for r, c in rock)
    if not hit_walls(blown_rock) \
            and not hit_resting_rocks(blown_rock, rested_rocks):
        rock = blown_rock
    fallen_rock = tuple((r - 1, c) for r, c in rock)
    if hit_floor(fallen_rock) \
            or hit_resting_rocks(fallen_rock, rested_rocks):
        rested = True
    else:
        rock = fallen_rock
    return rock, rested


def new_rock(shape, row):
    if shape == '-':
        return (
            (row + 0, 3), (row + 0, 4), (row + 0, 5), (row + 0, 6))
    elif shape == '+':
        return (
                          (row + 2, 4),
            (row + 1, 3), (row + 1, 4), (row + 1, 5),
                          (row + 0, 4))
    elif shape == 'J':
        return (
                                        (row + 2, 5),
                                        (row + 1, 5),
            (row + 0, 3), (row + 0, 4), (row + 0, 5))
    elif shape == '|':
        return (
            (row + 3, 3),
            (row + 2, 3),
            (row + 1, 3),
            (row + 0, 3))
    elif shape == 'o':
        return (
            (row + 1, 3), (row + 1, 4),
            (row + 0, 3), (row + 0, 4))


def make_state(peaks, shape, instruction_number):
    base = min(peaks)
    return tuple(p - base for p in peaks), shape, instruction_number


def main():
    instructions = parse_instructions('aoc17-input')
    shapes = itertools.cycle(('-', '+', 'J', '|', 'o'))
    peaks = [0 for _ in range(7)]
    rested_rocks = set()
    #N = 2022
    N = 1_000_000_000_000
    falling_rock = None
    rocks_count = 0
    states = {}
    jump_addition = 0
    jumped = False
    while N:
        if not falling_rock:
            shape = next(shapes)
            falling_rock = new_rock(shape, max(peaks) + 4)
        instruction_number, instruction = next(instructions)
        falling_rock, rested = update(falling_rock, instruction, rested_rocks)
        if rested:
            for r, c in falling_rock:
                peaks[c - 1] = max(peaks[c - 1], r)
                rested_rocks.add((r, c))
            rocks_count += 1
            N -= 1
            falling_rock = None
            if not jumped:
                state = make_state(peaks, shape, instruction_number)
                if state not in states:
                    states[state] = (rocks_count, max(peaks))
                else:  # found a cycle
                    prev_rocks_count, prev_top_row = states[state]
                    cycle_length = rocks_count - prev_rocks_count
                    cycle_addition = max(peaks) - prev_top_row
                    jump_addition = (N // cycle_length) * cycle_addition
                    rocks_count += cycle_length * (N // cycle_length)
                    N = N % cycle_length
                    jumped = True
    print(f'Rocks count: {rocks_count}\nTop row: {max(peaks) + jump_addition}')


try:
    main()
except KeyboardInterrupt:
    pass
