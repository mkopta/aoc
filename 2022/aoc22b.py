#!/usr/bin/env python3.11
import re
import time

HEADINGS = ['>', 'v', '<', '^']


def parse_path_description(raw_path_description):
    raw_path_description = raw_path_description.strip()
    steps = re.findall(r'\d+', raw_path_description)
    turns = re.findall(r'\D+', raw_path_description)
    path_description = []
    while steps or turns:
        if steps:
            path_description.append(int(steps.pop(0)))
        if turns:
            path_description.append(turns.pop(0))
    return path_description


def parse_board(raw_board):
    open_tiles, wall_tiles, init = set(), set(), None
    row = 1
    for line in raw_board.split('\n'):
        col = 1
        for tile in line:
            if tile == '.':
                if not init:
                    init = (row, col)
                open_tiles.add((row, col))
            elif tile == '#':
                wall_tiles.add((row, col))
            col += 1
        row += 1
    return open_tiles, wall_tiles, init


def parse_input(filename):
    raw_board, raw_path_description = open(filename).read().split('\n\n')
    path_description = parse_path_description(raw_path_description)
    open_tiles, wall_tiles, current_position = parse_board(raw_board)
    return path_description, open_tiles, wall_tiles, current_position


def change_heading(heading, turn):
    idx = HEADINGS.index(heading)
    match turn:
        case 'R': idx = (idx + 1) % len(HEADINGS)
        case 'L': idx -= 1
    return HEADINGS[idx]


def increment_position(prev_position, heading):
    next_row, next_col = row, col = prev_position
    match heading:
        case '>': next_col += 1
        case 'v': next_row += 1
        case '<': next_col -= 1
        case '^': next_row -= 1
    next_position = next_row, next_col
    return next_position


def oposite_heading(heading):
    match heading:
        case '>': return '<'
        case '<': return '>'
        case '^': return 'v'
        case 'v': return '^'


def wrap_position(position, heading, open_tiles, wall_tiles):
    # cols:  (1, 50)   (51, 100)   (101, 150)
    # rows:
    # (  1,  50)
    # ( 51, 100)
    # (101, 150)
    # (151, 200)
    row, col = position
    if   heading == '^' and row ==   1 and  51 <= col <= 100:  # D --> E
        return '>', (col + 100, 1)
    elif heading == '^' and row ==   1 and 101 <= col <= 150:  # F --> E
        return '^', (200, col - 100)
    elif heading == '^' and row == 101 and   1 <= col <=  50:  # C --> B
        return '>', (col + 50, 51)

    elif heading == 'v' and row ==  50 and 101 <= col <= 150:  # F --> B
        return '<', (col - 50, 100)
    elif heading == 'v' and row == 150 and  51 <= col <= 100:  # A --> E
        return '<', (col + 100, 50)
    elif heading == 'v' and row == 200 and   1 <= col <=  50:  # E --> F
        return 'v', (1, col + 100)

    elif heading == '>' and   1 <= row <=  50 and col == 150:  # F --> A
        return '<', ((51 - row) + 100, 100)
    elif heading == '>' and  51 <= row <= 100 and col == 100:  # B --> F
        return '^', (50, row + 50)
    elif heading == '>' and 101 <= row <= 150 and col == 100:  # A --> F
        return '<', (51 - (row - 100), 150)
    elif heading == '>' and 151 <= row <= 200 and col ==  50:  # E --> A
        return '^', (150, row - 100)

    elif heading == '<' and   1 <= row <=  50 and col ==  51:  # D --> C
        return '>', ((51 - row) + 100, 1)
    elif heading == '<' and  51 <= row <= 100 and col ==  51:  # B --> C
        return 'v', (101, row - 50)
    elif heading == '<' and 101 <= row <= 150 and col ==   1:  # C --> D
        return '>', (51 - (row - 100), 51)
    elif heading == '<' and 151 <= row <= 200 and col ==   1:  # E --> D
        return 'v', (1, row - 100)


def move_by_one_step(prev_position, prev_heading, open_tiles, wall_tiles):
    next_position = increment_position(prev_position, prev_heading)
    if next_position in open_tiles:
        # we can move there
        return prev_heading, next_position
    elif next_position in wall_tiles:
        # we are stuck behind a wall
        return prev_heading, prev_position
    else:
        # we are out-of-bounds, have to wrap around check if we can go there
        next_heading, next_position = wrap_position(
            prev_position, prev_heading, open_tiles, wall_tiles)
        if next_position in open_tiles:
            return next_heading, next_position
        elif next_position in wall_tiles:
            return prev_heading, prev_position


path_description, open_tiles, wall_tiles, current_position \
    = parse_input('aoc22-input')
heading = '>'
for steps in path_description:
    if steps in ('L', 'R'):
        heading = change_heading(heading, steps)
    else:
        for _ in range(steps):
            heading, current_position = move_by_one_step(
                current_position, heading, open_tiles, wall_tiles)
row, col = current_position
print(1000 * row + 4 * col + HEADINGS.index(heading))
