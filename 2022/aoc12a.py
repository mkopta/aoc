#!/usr/bin/env python3
import curses


def print_area(area, stdscr):
    for y in range(len(area)):
        for x in range(len(area[y])):
            stdscr.addstr(y, x, area[y][x], curses.A_DIM)


def expand(xy, area):
    x, y = xy
    h = ord(area[y][x] if area[y][x] != 'S' else 'a')
    exys = []
    if y < len(area) - 1 and ord(area[y+1][x]) <= h+1:
        exys.append((x, y+1))
    if y > 0 and ord(area[y-1][x]) <= h+1:
        exys.append((x, y-1))
    if x < len(area[0])-1 and ord(area[y][x+1]) <= h+1:
        exys.append((x+1, y))
    if x > 0 and ord(area[y][x-1]) <= h+1:
        exys.append((x-1, y))
    return exys


def init():
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)


def cleanup():
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()


def main(stdscr):
    stdscr.clear()
    area = []
    for line in open('aoc12-input').read().strip().split():
        area.append([c for c in line])
    for y in range(len(area)):
        for x in range(len(area[y])):
            if area[y][x] == 'S':
                S = (x, y)
            elif area[y][x] == 'E':
                E = (x, y)
    queue = [S]
    visited = set()
    parents = {S: None}
    print_area(area, stdscr)
    while queue:
        xy = queue.pop(0)
        visited.add(xy)
        x, y = xy
        stdscr.addstr(y, x, area[y][x])
        if xy == E:
            break
        for exy in expand(xy, area):
            if exy not in visited and exy not in queue:
                queue.append(exy)
                parents[exy] = xy
        stdscr.refresh()
    xy = E
    print_area(area, stdscr)
    steps = 0
    while xy:
        x, y = xy
        stdscr.addstr(y, x, area[y][x])
        stdscr.refresh()
        xy = parents[xy]
        steps += 1

    stdscr.addstr(len(area) + 1, 0, f'Steps needed: {steps - 1}')
    stdscr.getch()


curses.wrapper(main)
