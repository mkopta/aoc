#!/usr/bin/env python3.11


def snafu_to_decimal(snafu):
    decimal = 0
    i = len(snafu) - 1
    for n in snafu:
        match n:
            case '-': n = -1
            case '=': n = -2
            case n: n = int(n)
        decimal += n * (5 ** i)
        i -= 1
    return decimal


def decimal_to_snafu(decimal):
    snafu = ''

    i = 0
    while True:
        if ((5 ** i) * 2) >= decimal:
            break
        i += 1

    while i >= 0:
        options = {
            '=': decimal + (5 ** i) * 2,
            '-': decimal + (5 ** i),
            '0': decimal,
            '1': decimal - (5 ** i),
            '2': decimal - (5 ** i) * 2}
        option = '='
        lowest = options[option]
        for s in ('-', '0', '1', '2'):
            if abs(options[s]) < lowest:
                option = s
                lowest = abs(options[s])
        decimal = options[option]
        snafu += option
        i -= 1

    return snafu


total_in_decimal = 0
for snafu in open('aoc25-input'):
    snafu = snafu.rstrip()
    total_in_decimal += snafu_to_decimal(snafu)
print(total_in_decimal, decimal_to_snafu(total_in_decimal))
