#!/usr/bin/env python3
contained_pairs_count = 0

def range_to_set(r):
    """ '4-8' --> {4, 5, 6, 7, 8} """
    a, b = map(int, r.split('-'))
    return set(range(a, b + 1))

with open('aoc04-input', 'r') as f:
    lines = f.readlines()
for line in lines:
    line = line.strip()
    l, r = line.split(',')
    lset = range_to_set(l)
    rset = range_to_set(r)
    if lset & rset:
        contained_pairs_count += 1
print(contained_pairs_count)
