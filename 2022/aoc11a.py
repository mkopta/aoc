#!/usr/bin/env python3


def parse_monkeys(content):
    monkeys = []
    for section in content.split('\n\n'):
        lines = section.split('\n')
        monkey = {}
        monkey['number'] = int(lines[0].replace(':', '').split()[1])
        monkey['items'] = list(map(int, lines[1].split(': ')[1].split(', ')))
        monkey['operation'] = lines[2].split('= ')[1]
        monkey['test'] = {
            'divisible by': int(lines[3].split()[-1]),
            'pass': int(lines[4].split()[-1]),
            'fail': int(lines[5].split()[-1])}
        monkey['inspected_items_count'] = 0
        monkeys.append(monkey)
    return monkeys


def do_operation(op, item):
    return eval(op.replace('old', str(item)))


def throw_item_to(item, monkey_number, monkeys):
    for monkey in monkeys:
        if monkey['number'] == monkey_number:
            monkey['items'].append(item)
            return


def do_round(monkeys):
    for monkey in monkeys:
        for item in monkey['items']:
            item = do_operation(monkey['operation'], item) // 3
            if item % monkey['test']['divisible by'] == 0:
                throw_item_to(item, monkey['test']['pass'], monkeys)
            else:
                throw_item_to(item, monkey['test']['fail'], monkeys)
            monkey['inspected_items_count'] += 1
        monkey['items'] = []


def print_items(monkeys):
    for m in monkeys:
        print(f'Monkey {m["number"]}: {", ".join(map(str, m["items"]))}')


monkeys = parse_monkeys(open('aoc11-input').read())
for i in range(20):
    print(f'Round {i+1}')
    do_round(monkeys)
    print_items(monkeys)
    print()
for monkey in monkeys:
    print(
        f'Monkey {monkey["number"]} inspected items'
        f' {monkey["inspected_items_count"]} times.')

activity = sorted(
    (m['inspected_items_count'] for m in monkeys), reverse=True)

print(f'\nMonkey business: {activity[0] * activity[1]}')
