#!/usr/bin/env python3
#
#            y = -1
#   x = -1  x,y=  0  x =  1
#            y =  1
#
import itertools


class Point:
    def __init__(self):
        self.x = 0
        self.y = 0

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f'x={self.x} y={self.y}'

    def is_adjacent(self, other):
        return abs(self.x - other.x) < 2 and abs(self.y - other.y) < 2

    def move_towards(self, other):
        if self.x < other.x:
            self.x += 1
        if self.y < other.y:
            self.y += 1
        if self.x > other.x:
            self.x -= 1
        if self.y > other.y:
            self.y -= 1

    def move(self, direction):
        if direction == 'U':
            self.y -= 1
        elif direction == 'D':
            self.y += 1
        elif direction == 'L':
            self.x -= 1
        elif direction == 'R':
            self.x += 1


knots = [Point() for _ in range(10)]
tail_visited_xys = {(knots[-1].x, knots[-1].y)}
with open('aoc09-input', 'r') as f:
    for instruction in f.readlines():
        direction, count = instruction.split()
        for _ in range(int(count)):
            knots[0].move(direction)  # move head
            for knot1, knot2 in itertools.pairwise(knots):
                if knot1 == knot2 or knot2.is_adjacent(knot1):
                    continue
                knot2.move_towards(knot1)
            tail_visited_xys.add((knots[-1].x, knots[-1].y))
print(len(tail_visited_xys))
