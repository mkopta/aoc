#!/usr/bin/env python3

def compare(l, r, indent=''):
    print(f'{indent}- Compare {l} vs {r}')
    if type(l) is list and type(r) is list:
        while True:
            if not l and not r:
                return 0
            if not l and r:
                print(f'{indent}  - Left side ran out of items')
                return -1
            if l and not r:
                print(f'{indent}  - Right side ran out of items')
                return 1
            lv = l.pop(0)
            rv = r.pop(0)
            if rv := compare(lv, rv, indent=indent + '  '):
                return rv
    elif type(l) is int and type(r) is int:
        if l > r:
            print(f'{indent}  - Right side is smaller')
            return 1
        elif l < r:
            print(f'{indent}  - Left side is smaller')
            return -1
        else:
            return 0
    else:  # mixed types
        if type(l) is int:
            print(f'{indent}  - Mixed types, converting left to {[l]}')
            return compare([l], r, indent=indent + '  ')
        else:
            print(f'{indent}  - Mixed types, converting right to {[r]}')
            return compare(l, [r], indent=indent + '  ')


index = 1
indices_of_packet_pairs_in_right_order = []
for block in open('aoc13-input').read().strip().split('\n\n'):
    left, right = map(eval, block.split('\n'))
    print(f'== Pair {index} ==')
    if compare(left, right) != 1:
        indices_of_packet_pairs_in_right_order.append(index)
    index += 1
    print()
print(sum(indices_of_packet_pairs_in_right_order))
