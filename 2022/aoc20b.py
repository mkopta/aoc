#!/usr/bin/env python3.11
i = 0
numbers = []
for n in open('aoc20-input').read().strip().split('\n'):
    numbers.append((int(n) * 811589153, i))
    i += 1
original = tuple(numbers)
for _ in range(10):
    for (n, i) in original:
        idx = numbers.index((n, i))
        new_idx = idx + n
        if new_idx < 0:
            new_idx = new_idx % - (len(numbers) - 1)
            new_idx = len(numbers) - (- new_idx)
            new_idx -= 1
        elif new_idx >= len(numbers):
            new_idx = new_idx % (len(numbers) - 1)
        numbers.remove((n, i))
        numbers.insert(new_idx, (n, i))
numbers = [n for n, _ in numbers]
zero_idx = numbers.index(0)
n1 = numbers[ (zero_idx + 1000) % len(numbers)  ]
n2 = numbers[ (zero_idx + 2000) % len(numbers)  ]
n3 = numbers[ (zero_idx + 3000) % len(numbers)  ]
print(n1, n2, n3)
print(n1 + n2 + n3)
