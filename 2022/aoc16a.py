#!/usr/bin/env python3.11
# example: 1651
# input: 1580
import itertools
from copy import copy
from pprint import pprint


def load_data(filename):
    flows = {}
    graph = {}
    for line in open(filename):
        words = line.split()
        valve_name = words[1]
        flows[valve_name] = int(words[4].replace('rate=', '').replace(';', ''))
        graph[valve_name] = [w.replace(',', '') for w in words[9:]]
    return flows, graph


def calculate_paths_costs(graph, flows):
    """
    graph[node] = [node, node, node, ..]
    reduced_graph[node][node] = distance
    """
    non_zero_flow_valves = [v for v, f in flows.items() if f > 0]
    reduced_graph = {}
    for valve_name in non_zero_flow_valves + ['AA']:
        reduced_graph[valve_name] = {}
        queue = [(valve_name, 0, [])]
        visited = set()
        while queue:
            vp, distance, prevs = queue.pop(0)
            visited.add(vp)
            for v in graph[vp]:
                if v in (q[0] for q in queue) or v in visited:
                    continue
                queue.append((v, distance + 1, prevs + [vp]))
                reduced_graph[valve_name][v] = distance + 1
    return reduced_graph


flows, graph = load_data('aoc16-input')
paths_costs = calculate_paths_costs(graph, flows)
valves = [v for v, f in flows.items() if f > 0]  # ignoring jammed
valves.sort(key=lambda v: flows[v], reverse=True)  # best first
queue = [('AA', {}, 1, 0)]  # valve, valves_open_at, minute, pressure_released
best_strategy = None
max_pressure_released = 0
while queue:
    current_valve, valves_open_at, minute, pressure_released = queue.pop()
    if pressure_released > max_pressure_released:
        best_strategy = valves_open_at
        max_pressure_released = pressure_released
    if len(valves_open_at) == len(valves) or minute >= 29:
        continue
    for next_valve in valves:
        if next_valve in valves_open_at:
            continue
        dist = paths_costs[current_valve][next_valve]
        open_at = minute + dist + 1
        if open_at > 30:
            continue
        _valves_open_at = copy(valves_open_at)
        _valves_open_at[next_valve] = open_at
        queue.append((
            next_valve, 
            _valves_open_at,
            open_at,
            pressure_released + (31 - open_at) * flows[next_valve]))

print(max_pressure_released)
print(best_strategy)
