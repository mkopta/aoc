#!/usr/bin/env python3

def load_data():
    a, b = [], []
    with open(0) as f:
        for line in f:
            l, r = line.strip().split()
            a.append(int(l))
            b.append(int(r))
    return a, b


def calculate(a, b):
    a.sort()
    b.sort()
    sum = 0
    for n1, n2 in zip(a, b):
        sum += abs(n1 - n2)
    return sum


a, b = load_data()
sum = calculate(a, b)
print(sum)
