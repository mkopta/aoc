#!/usr/bin/env python3
import sys


def load_disk():
    disk_layout = list(map(int, open(0).read().strip()))
    disk, is_file, file_id = [], True, 0
    for block_length in disk_layout:
        disk.append((is_file, block_length, file_id if is_file else None))
        is_file = not is_file
        if is_file:
            file_id += 1
    return disk


def print_disk(disk):
    striping = True
    for is_file, length, file_id in disk:
        for _ in range(length):
            if is_file:
                print(f'\033[9{2 if striping else 3}m{file_id}\033[0m', end='')
                striping = not striping
            else:
                print('.', end='')
                striping = True
    print()


def calculate_checksum(disk):
    checksum, file_index = 0, 0
    for (is_file, block_length, file_id) in disk:
        if not is_file:
            file_index += block_length
            continue
        for i in range(file_index, file_index + block_length):
            checksum += i * file_id
        file_index += block_length
    return checksum


disk = load_disk()
#print_disk(disk)
*_, current_file_id = disk[-1]
while current_file_id > 0:
    for file_index in range(len(disk) - 1, -1, -1):
        is_file, file_length, file_id = disk[file_index]
        if is_file and file_id == current_file_id:
            break
    disk[file_index] = (False, file_length, None)
    for space_index, (is_file, block_length, _) in enumerate(disk):
        if is_file or block_length < file_length:
            continue
        disk[space_index] = (True, file_length, file_id)
        remaining_space = block_length - file_length
        if remaining_space > 0:
            disk.insert(space_index + 1, (False, remaining_space, None))
        break
    current_file_id -= 1
print(calculate_checksum(disk))
