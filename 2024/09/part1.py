#!/usr/bin/env python3


def load_disk():
    disk_layout = list(map(int, open(0).read().strip()))
    disk, is_file, file_id = [], True, 0
    for block_length in disk_layout:
        disk.extend([(is_file, file_id if is_file else None)] * block_length)
        is_file = not is_file
        if is_file:
            file_id += 1
    return disk


def print_disk(disk):
    striping = True
    for is_file, file_id in disk:
        if is_file:
            print(f'\033[9{2 if striping else 3}m{file_id}\033[0m', end='')
            striping = not striping
        else:
            print('.', end='')
            striping = True
    print()


def move_block(disk, ssi):
    moved = False
    for ssi in range(ssi, len(disk)):
        is_file = disk[ssi][0]
        if is_file:
            continue
        disk[ssi] = disk[-1]
        disk = disk[:-1]
        moved = True
        break
    return disk, moved, ssi


def calculate_checksum(disk):
    checksum = 0
    for i, (is_file, file_id) in enumerate(disk):
        if not is_file:
            continue
        checksum += i * file_id
    return checksum


disk = load_disk()
ssi = 0  # space search index
while True:
    disk, moved, ssi = move_block(disk, ssi)
    if not moved:
        break
print(calculate_checksum(disk))
