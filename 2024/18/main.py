#!/usr/bin/env python3

def load():
    positions = []
    for line in open(0):
        x, y = list(map(int, line.strip().split(',')))
        positions.append((x, y))
    return positions


def bfs(grid_size, obstacles, start, end):
    seen = {start}
    sx, sy = start
    q = [(sx, sy, 0)]
    while q:
        (x, y, s) = q.pop(0)
        for dx, dy in [(-1, 0), (0, 1), (1, 0), (0, -1)]:
            nx, ny, ns = x + dx, y + dy, s + 1
            if (nx, ny) == end:
                return ns
            if (
                    nx < 0 or ny < 0 or nx >= grid_size or ny >= grid_size
                    or (nx, ny) in seen
                    or (nx, ny) in obstacles):
                continue
            seen.add((nx, ny))
            q.append((nx, ny, ns))
    return 0


positions = load()
#grid_size = 7
grid_size = 71
#obstacles = set(positions[:12])
obstacles = set(positions[:1024])
steps = bfs(grid_size, obstacles, (0, 0), (grid_size - 1, grid_size - 1))
print('part1:', steps)
for n in range(1024 + 1, len(positions)):
    obstacles = set(positions[:n])
    steps = bfs(grid_size, obstacles, (0, 0), (grid_size - 1, grid_size - 1))
    if steps == 0:
        x, y = positions[n - 1]
        print(f'part2: {x},{y}')
        break
