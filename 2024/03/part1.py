#!/usr/bin/env python3
import re
content = open(0).read()
instructions = re.findall(r'mul\(\d+,\d+\)', content)
sum = 0
for instruction in instructions:
    a, b = re.findall(r'\d+', instruction)
    sum += int(a) * int(b)
print(sum) 
