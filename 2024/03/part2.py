#!/usr/bin/env python3
import re
content = open(0).read()
matches = re.findall(
    r'(don\'t\(\))|(do\(\))|(mul\(\d+,\d+\))',
    content)
instructions = []
for match in matches:
    for item in match:
        if item:
            instructions.append(item)
sum = 0
mul_enabled = True
for instruction in instructions:
    if instruction == 'do()':
        mul_enabled = True
    elif instruction == "don't()":
        mul_enabled = False
    elif mul_enabled:
        a, b = re.findall(r'\d+', instruction)
        sum += int(a) * int(b)
print(sum) 
