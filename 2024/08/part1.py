#!/usr/bin/env python3
import itertools
all_antennas = {}
for row, line in enumerate(open(0), 1):
    for col, char in enumerate(line.strip(), 1):
        if char == '.':
            continue
        if char in all_antennas:
            all_antennas[char].add((row, col))
        else:
            all_antennas[char] = {(row, col)}
max_row, max_col = row, col
antinodes = set()
for antenna_type, antennas in all_antennas.items():
    for a1, a2 in itertools.permutations(antennas, 2):
        (a1r, a1c), (a2r, a2c) = a1, a2
        dr = a2r - a1r
        dc = a2c - a1c
        anr = a1r + dr * 2
        anc = a1c + dc * 2
        if anr < 1 or anr > max_row or anc < 1 or anc > max_col:
            continue
        antinode = anr, anc
        antinodes.add(antinode)
print(len(antinodes))
