#!/usr/bin/env python3
from time import sleep
import re


def load():
    robots, velocities = [], []
    for line in open(0):
        m = re.search(r'^p=(\d+),(\d+) v=(-?\d+),(-?\d+)$', line)
        if not m:
            raise Exception('Failed to parse')
        p = (int(m[1]), int(m[2]))
        v = (int(m[3]), int(m[4]))
        robots.append(p)
        velocities.append(v)
    return robots, velocities


def print_robots(robots, W, H):
    for y in range(H):
        for x in range(W):
            count = robots.count((x, y))
            print('.' if count == 0 else str(count), end='')
        print()


def part1(robots, velocities, W, H):
    new_positions = []
    for (px, py), (dx, dy) in zip(robots, velocities):
        px = (px + dx * 100) % W
        py = (py + dy * 100) % H
        new_positions.append((px, py))
    q1 = 0
    for y in range(H // 2):
        for x in range(W // 2):
            q1 += new_positions.count((x, y))
    q2 = 0
    for y in range(H // 2):
        for x in range(W // 2 + 1, W):
            q2 += new_positions.count((x, y))
    q3 = 0
    for y in range(H // 2 + 1, H):
        for x in range(W // 2):
            q3 += new_positions.count((x, y))
    q4 = 0
    for y in range(H // 2 + 1, H):
        for x in range(W // 2 + 1, W):
            q4 += new_positions.count((x, y))
    return q1 * q2 * q3 * q4


def contains_tree(robots, W):
    mirrored = 0
    for x, y in robots:
        if ((W - 1 - x), y) in robots:
            mirrored += 1
    # more than 25% of bots are making a mirrored image
    return mirrored > (len(robots) // 4)


def part2(robots, velocities, W, H):
    second = 0
    while True:
        second += 1
        for i, ((px, py), (dx, dy)) in enumerate(zip(robots, velocities)):
            robots[i] = (px + dx) % W, (py + dy) % H
        if contains_tree(robots, W):
            return second


#W, H = 11, 7
W, H = 101, 103
robots, velocities = load()
print('part1:', part1(robots, velocities, W, H))
print('part2:', part2(robots, velocities, W, H))
print_robots(robots, W, H)
