#!/usr/bin/env python3
import itertools

total = 0
for line in open(0).read().splitlines():
    result, numbers = line.split(': ')
    result = int(result)
    numbers = list(map(int, numbers.split()))
    positions = len(numbers) - 1
    for operations in itertools.product(*['*+'] * positions):
        res = numbers[0]
        for op, n in zip(operations, numbers[1:]):
            if op == '*':
                res *= n
            elif op == '+':
                res += n
        if res == result:
            total += result
            break
print(total)
