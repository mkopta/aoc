#!/usr/bin/env python3
chars = {}
row, col = 1, 1
for line in open(0):
    col = 1
    for char in line.strip():
        chars[(row, col)] = char
        col += 1
    row += 1
count = 0
for (r, c), char in chars.items():
    if char != 'X':
        continue
    # ↑
    if chars.get((r-1, c)) == 'M' and chars.get((r-2, c)) == 'A' and chars.get((r-3, c)) == 'S':
        count += 1
    # ↗
    if chars.get((r-1, c+1)) == 'M' and chars.get((r-2, c+2)) == 'A' and chars.get((r-3, c+3)) == 'S':
        count += 1
    # →
    if chars.get((r, c+1)) == 'M' and chars.get((r, c+2)) == 'A' and chars.get((r, c+3)) == 'S':
        count += 1
    # ↘
    if chars.get((r+1, c+1)) == 'M' and chars.get((r+2, c+2)) == 'A' and chars.get((r+3, c+3)) == 'S':
        count += 1
    # ↓
    if chars.get((r+1, c)) == 'M' and chars.get((r+2, c)) == 'A' and chars.get((r+3, c)) == 'S':
        count += 1
    # ↙
    if chars.get((r+1, c-1)) == 'M' and chars.get((r+2, c-2)) == 'A' and chars.get((r+3, c-3)) == 'S':
        count += 1
    # ←
    if chars.get((r, c-1)) == 'M' and chars.get((r, c-2)) == 'A' and chars.get((r, c-3)) == 'S':
        count += 1
    # ↖
    if chars.get((r-1, c-1)) == 'M' and chars.get((r-2, c-2)) == 'A' and chars.get((r-3, c-3)) == 'S':
        count += 1
print(count)
