#!/usr/bin/env python3
chars = {}
row, col = 1, 1
for line in open(0):
    col = 1
    for char in line.strip():
        chars[(row, col)] = char
        col += 1
    row += 1
count = 0
for (r, c), char in chars.items():
    if char != 'A':
        continue
    # M ↗ S || S ↗ M
    if not ((chars.get((r-1, c+1)) == 'M' and chars.get((r+1, c-1)) == 'S')
            or
            (chars.get((r-1, c+1)) == 'S' and chars.get((r+1, c-1)) == 'M')):
        continue
    # M ↘ S || S ↘ M
    if not ((chars.get((r+1, c+1)) == 'M' and chars.get((r-1, c-1)) == 'S')
            or
            (chars.get((r+1, c+1)) == 'S' and chars.get((r-1, c-1)) == 'M')):
        continue
    count += 1
print(count)
