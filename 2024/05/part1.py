#!/usr/bin/env python3
rules, updates = open(0).read().split('\n\n')
order = {}  # a page (key) comes before pages (value)
for rule in rules.splitlines():
    x, y = list(map(int, rule.strip().split('|')))
    successors = order.get(x, set())
    successors.add(y)
    order[x] = successors
valid_updates = []
for update in updates.splitlines():
    pages_to_print = list(map(int, update.strip().split(',')))
    already_printed_pages = set()
    for page in pages_to_print:
        if order.get(page, set()).intersection(already_printed_pages):
            # this update is out of order
            break
        already_printed_pages.add(page)
    else:
        valid_updates.append(pages_to_print)
total = 0
for update in valid_updates:
    total += update[len(update) // 2]
print(total)
