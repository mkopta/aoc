#!/usr/bin/env python3

def load():
    rules, updates = open(0).read().split('\n\n')
    order = {}  # a page (key) comes before pages (value)
    for rule in rules.splitlines():
        x, y = list(map(int, rule.strip().split('|')))
        successors = order.get(x, set())
        successors.add(y)
        order[x] = successors
    updates = [list(map(int, u.split(','))) for u in updates.splitlines()]
    return order, updates


def verify(order, update):
    already_printed_pages = set()
    for i, page in enumerate(update):
        if order.get(page, set()).intersection(already_printed_pages):
            return False, i
        already_printed_pages.add(page)
    return True, -1


def move_page_back_by_one(pages, i):
    pages[i - 1], pages[i] = pages[i], pages[i - 1]


total = 0
order, updates = load()
for update in updates:
    is_in_order, bad_page_index = verify(order, update)
    if is_in_order:
        continue
    while True:
        is_in_order, bad_page_index = verify(order, update)
        if is_in_order:
            break
        move_page_back_by_one(update, bad_page_index)
    total += update[len(update) // 2]
print(total)
