#!/usr/bin/env python3
import re


def load():
    machines = []
    for section in open(0).read().split('\n\n'):
        machine = {}
        for line in section.split('\n'):
            if m := re.search(r'^Button (A|B): X\+(\d+), Y\+(\d+)$', line):
                machine[m[1]] = (int(m[2]), int(m[3]))
            elif m := re.search(r'^Prize: X=(\d+), Y=(\d+)', line):
                machine['Prize'] = (int(m[1]), int(m[2]))
        machines.append(machine)
    return machines


def brute_force(machine):
    for a in range(101):
        for b in range(101):
            ax, ay = machine['A']
            bx, by = machine['B']
            px, py = machine['Prize']
            if a * ax + b * bx == px and a * ay + b * by == py:
                return a, b
    return None


def calculate(machine):
    px, py = machine['Prize']
    ax, ay = machine['A']
    bx, by = machine['B']
    a = int((py * bx - px * by) / (ay * bx - ax * by))
    b = int((px - a * ax) / bx)
    if (a * ax + b * bx == px) and (a * ay + b * by == py):
        return a, b
    return None


machines = load()
total = 0
for machine in machines:
    #if solution := brute_force(machine):
    if solution := calculate(machine):
        a, b = solution
        total += a * 3 + b * 1
print('part1:', total)
total = 0
for machine in machines:
    px, py = machine['Prize']
    machine['Prize'] = (px + 10000000000000, py + 10000000000000)
    if solution := calculate(machine):
        a, b = solution
        total += a * 3 + b * 1
print('part2:', total)
