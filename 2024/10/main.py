#!/usr/bin/env python3

def load():
    positions = dict()
    trail_heads = set()
    for row, line in enumerate(open(0).read().splitlines(), 1):
        for col, char in enumerate(line, 1):
            if char == '0':
                trail_heads.add((row, col))
            elif char == '.':
                continue
            else:
                positions[(row, col)] = int(char)
    return positions, trail_heads


def expand(position, level, positions):
    r, c = position
    for next_position in ((r+1, c), (r-1, c), (r, c + 1), (r, c - 1)):
        if next_position not in positions:
            continue
        if positions[next_position] != level + 1:
            continue
        yield next_position


def calculate_trail_score(trail_head, positions, multiple=False):
    score = 0
    visited = set()
    queue = [(trail_head, 0)]
    while queue:
        position, level = queue.pop(0)
        if level == 9:
            score += 1
            continue
        for next_position in expand(position, level, positions):
            if not multiple and (next_position, level + 1) in visited:
                continue
            queue.append((next_position, level + 1))
            visited.add((next_position, level + 1))
    return score


positions, trail_heads = load()
total = 0
for trail_head in trail_heads:
    total += calculate_trail_score(trail_head, positions)
print('part1: ', total)
total = 0
for trail_head in trail_heads:
    total += calculate_trail_score(trail_head, positions, multiple=True)
print('part1: ', total)
