#!/usr/bin/env python3
from functools import cache


def transform(stone):
    if stone == 0:
        return [1]
    sstone = str(stone)
    if len(sstone) % 2 == 0:
        return [
            int(sstone[:len(sstone)//2]),
            int(sstone[len(sstone)//2:])]
    return [stone * 2024]


@cache
def count_stones(stone, blinks):
    if blinks == 0:
        return 1
    new_stones = transform(stone)
    count = 0
    for new_stone in new_stones:
        count += count_stones(new_stone, blinks - 1)
    return count


stones = [int(n) for n in open(0).read().strip().split()]
print('part1: ', sum(count_stones(s, 25) for s in stones))
print('part2: ', sum(count_stones(s, 75) for s in stones))
