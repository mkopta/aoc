#!/usr/bin/env python3
from functools import cache


def load():
    patterns = []
    first, second = open(0).read().split('\n\n')
    towels = first.split(', ')
    patterns = second.split()
    return towels, patterns


@cache
def count_solutions(pattern):
    count = 0
    for towel in AVAILABLE_TOWELS:
        if not pattern.startswith(towel):
            continue
        remaining_pattern = pattern[len(towel):]
        if not remaining_pattern:
            count += 1
            continue
        count += count_solutions(remaining_pattern)
    return count


AVAILABLE_TOWELS, patterns = load()

part1, part2 = 0, 0
for pattern in patterns:
    count = count_solutions(pattern)
    part1 += 1 if count else 0
    part2 += count
print('part1:', part1)
print('part2:', part2)
