def load():
    start, end, track = None, None, set()
    for row, line in enumerate(open(0), 1):
        for col, char in enumerate(line.strip(), 1):
            if char == '.':
                track.add((row, col))
            elif char == 'E':
                end = (row, col)
                track.add((row, col))
            elif char == 'S':
                start = (row, col)
                track.add((row, col))
    return start, end, track


def calculate_distances_to_end(start, end, track):
    d = 0
    distances_to_end = {end: 0}
    next_tile = {}
    r, c = end
    while (r, c) != start:
        for nr, nc in ((r - 1, c), (r, c + 1), (r + 1, c), (r, c - 1)):
            if (nr, nc) not in track: continue
            if (nr, nc) in distances_to_end: continue  # seen
            d += 1
            distances_to_end[(nr, nc)] = d
            next_tile[(nr, nc)] = r, c
            r, c = nr, nc
            break
    return distances_to_end, next_tile


def find_shortcuts(start, end, track, distances_to_end, next_tile):
    r, c = start
    shortcuts = {}
    while (r, c) != end:
        for dr, dc in ((-1, 0), (0, 1), (1, 0), (0, -1)):
            if ((r + dr), (c + dc)) in track: continue
            if ((r + dr * 2), (c + dc * 2)) not in track: continue
            sr, sc = (r + dr * 2), (c + dc * 2)
            d1 = distances_to_end[(r, c)]
            d2 = distances_to_end[(sr, sc)]
            save = d1 - (d2 + 2)
            if save > 0:
                if save not in shortcuts:
                    shortcuts[save] = 0
                shortcuts[save] += 1
        r, c = next_tile[(r, c)]
    return shortcuts


start, end, track = load()
distances_to_end, next_tile = calculate_distances_to_end(start, end, track)
shortcuts = find_shortcuts(start, end, track, distances_to_end, next_tile)
total = 0
for saved_time, count in sorted(shortcuts.items()):
    if saved_time >= 100:
        total += count
print(total)
