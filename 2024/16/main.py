#!/usr/bin/env python3
from queue import PriorityQueue


def load():
    walls = set()
    start, end = None, None
    for row, line in enumerate(open(0), 1):
        for col, char in enumerate(line.strip(), 1):
            if char == '#':
                walls.add((row, col))
            elif char == 'S':
                start = (row, col)
            elif char == 'E':
                end = (row, col)
    return walls, start, end, (row, col)


def print_maze(walls, start, end, max_row, max_col):
    for row in range(1, max_row + 1):
        for col in range(1, max_col + 1):
            if (row, col) in walls:
                print('\033[30;1;4m█\033[0m', end='')
            elif (row, col) == start:
                print('S', end='')
            elif (row, col) == end:
                print('E', end='')
            else:
                print(' ', end='')
        print()

def turns(direction):
    match direction:
        case (0, 1) | (0, -1): return (-1, 0), (1, 0)
        case (1, 0) | (-1, 0): return (0, 1), (0, -1)


def find_best_path(walls, start, end):
    visited = {}
    q = PriorityQueue()
    q.put((0, start, (0, 1)))
    while not q.empty():
        score, position, facing = q.get()
        # option 1: continue in the same direction
        pr, pc = position
        dr, dc = facing
        new_position = pr + dr, pc + dc
        if new_position not in walls:
            if new_position == end:
                return score + 1
            if new_position not in visited \
                    or visited[(new_position, facing)] > score + 1:
                q.put((score + 1, new_position, facing))
                visited[(new_position, facing)] = score + 1
        # option 2, 3: rotate in two direction
        for facing in turns(facing):
            if (position, facing) not in visited \
                    or visited[(position, facing)] > score + 1000:
                q.put((score + 1000, position, facing))
                visited[(position, facing)] = score + 1000


walls, start, end, (max_row, max_col) = load()
#print_maze(walls, start, end, max_row, max_col)
score = find_best_path(walls, start, end)
print(score)
