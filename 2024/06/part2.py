#!/usr/bin/env python3

def load():
    obstacles = set()
    guard = None
    for row, line in enumerate(open(0), 1):
        for col, char in enumerate(line.strip(), 1):
            if char == '.':
                continue
            if char == '^':
                guard = (row, col)
            if char == '#':
                obstacles.add((row, col))
    return obstacles, guard, (row, col)


def add_coordinates(one, two):
    one_row, one_col = one
    two_row, two_col = two
    return one_row + two_row, one_col + two_col


def is_out_of_bounds(guard, dimensions):
    max_row, max_col = dimensions
    row, col = guard
    return row > max_row or col > max_col \
        or row < 1 or col < 1


def rotate_to_right(direction):
    if direction == (-1, 0):
        return 0, 1
    if direction == (0, 1):
        return 1, 0
    if direction == (1, 0):
        return 0, -1
    if direction == (0, -1):
        return -1, 0


def is_stuck_in_loop(obstacles, guard, dimensions):
    direction = (-1, 0)
    visited = set()
    while True:
        visited.add((direction, guard))
        next_step = add_coordinates(direction, guard)
        while next_step in obstacles:
            direction = rotate_to_right(direction)
            next_step = add_coordinates(direction, guard)
        if is_out_of_bounds(next_step, dimensions):
            return False
        if (direction, next_step) in visited:
            return True
        guard = next_step


obstacles, guard, dimensions = load()
max_row, max_col = dimensions
count = 0
for r in range(1, max_row + 1):
    for c in range(1, max_col + 1):
        if (r, c) in obstacles or (r, c) == guard:
            continue
        if is_stuck_in_loop(obstacles | {(r, c)}, guard, dimensions):
            count += 1
print('\n' + str(count))
