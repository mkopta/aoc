#!/usr/bin/env python3
the_map = {}
for row, line in enumerate(open(0)):
    for col, char in enumerate(line.strip()):
        the_map[(row, col)] = char
price = 0
while the_map:
    plot, plot_type = list(the_map.items())[0]
    region_perimeter, region_area = 0, 0
    region = set()
    plots_to_visit = {plot}
    del the_map[plot]
    while plots_to_visit:
        r, c = plots_to_visit.pop()
        region_area += 1
        region.add((r, c))
        for next_plot in [(r - 1, c), (r, c + 1), (r + 1, c), (r, c - 1)]:
            if next_plot in region or next_plot in plots_to_visit:
                continue
            if next_plot not in the_map or the_map[next_plot] != plot_type:
                region_perimeter += 1
                continue
            plots_to_visit.add(next_plot)
            del the_map[next_plot]
    #print(f'{plot_type=}, {region_perimeter=}, {region_area=}')
    price += region_perimeter * region_area
print(price)
