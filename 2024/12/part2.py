#!/usr/bin/env python3
the_map = {}
for row, line in enumerate(open(0), 1):
    for col, char in enumerate(line.strip(), 1):
        the_map[(row, col)] = char
price = 0
while the_map:
    plot, plot_type = list(the_map.items())[0]
    region_perimeter, region_area = [], 0
    region = set()
    plots_to_visit = {plot}
    del the_map[plot]
    while plots_to_visit:
        r, c = plots_to_visit.pop()
        region_area += 1
        region.add((r, c))
        for next_plot in [(r - 1, c), (r, c + 1), (r + 1, c), (r, c - 1)]:
            if next_plot in region or next_plot in plots_to_visit:
                continue
            if next_plot not in the_map or the_map[next_plot] != plot_type:
                region_perimeter.append(((r, c), next_plot))
                continue
            plots_to_visit.add(next_plot)
            del the_map[next_plot]
    number_of_sides = 0
    while region_perimeter:
        (ir, ic), (er, ec) = region_perimeter.pop()  # inner, external
        number_of_sides += 1
        if (
                ((ir - 1, ic), (er - 1, ec)) in region_perimeter
                or
                ((ir + 1, ic), (er + 1, ec)) in region_perimeter):
            # vertical
            for dr in (-1, 1):
                nir = ir + dr
                ner = er + dr
                while ((nir, ic), (ner, ec))  in region_perimeter:
                    region_perimeter.remove(((nir, ic), (ner, ec)))
                    nir = nir + dr
                    ner = ner + dr
        elif (
                ((ir, ic + 1), (er, ec + 1)) in region_perimeter
                or
                ((ir, ic - 1), (er, ec - 1)) in region_perimeter):
            # horizontal
            for dc in (-1, 1):
                nic = ic + dc
                nec = ec + dc
                while ((ir, nic), (er, nec)) in region_perimeter:
                    region_perimeter.remove(((ir, nic), (er, nec)))
                    nic = nic + dc
                    nec = nec + dc
        else:  # singular
            continue
    region_price = number_of_sides * region_area
    print(
        f'A region of {plot_type} plants with price'
        f' {region_area} * {number_of_sides} = {region_price}')
    price += region_price
print(price)
