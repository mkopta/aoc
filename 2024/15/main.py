#!/usr/bin/env python3


def load():
    the_map = {}
    robot = None
    serialized_map, moves = open(0).read().split('\n\n')
    for row, line in enumerate(serialized_map.split('\n'), 1):
        for col, char in enumerate(line.strip(), 1):
            if char == '@':
                robot = (row, col)
            the_map[(row, col)] = char
    moves = moves.replace('\n', '')
    return the_map, robot, moves, (row, col)


def print_state(robot, the_map, dimensions):
    height, width = dimensions
    for row in range(1, height + 1):
        for col in range(1, width + 1):
            print(the_map[(row, col)], end='')
        print()
    

def move(item, direction, the_map):
    dr, dc = {
        '<': (0, -1),
        '>': (0, 1),
        '^': (-1, 0),
        'v': (1, 0)}[direction]
    ir, ic = item
    target = (ir + dr, ic + dc)
    if the_map[target] == '#':
        return False
    if the_map[target] == '.' or move(target, direction, the_map):
        the_map[target] = the_map[item]
        the_map[item] = '.'
        return target
    return False


the_map, robot, moves, dimensions = load()
for direction in moves:
    if new_position := move(robot, direction, the_map):
        robot = new_position

total = 0
for (r, c) in the_map:
    if the_map[(r, c)] != 'O':
        continue
    total += (r - 1) * 100 + (c - 1)
print(total)
