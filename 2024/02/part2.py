#!/usr/bin/env python3

def is_report_safe(report):
    direction = None
    for n1, n2 in zip(report[:-1], report[1:]):
        if not (1 <= abs(n1 - n2) <= 3):
            return False
        if not direction:
            direction = 'increasing' if n2 > n1 else 'decreasing'
        elif direction == 'increasing' and n1 > n2:
            return False
        elif direction == 'decreasing' and n1 < n2:
            return False
    return True


count = 0
for line in open(0):
    report = list(map(int, line.strip().split()))
    if is_report_safe(report):
        count += 1
    else:
        for i in range(len(report)):
            if is_report_safe(report[:i] + report[i+1:]):
                count +=1 
                break

print(count)
