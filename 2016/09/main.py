#!/usr/bin/env python3


def decompress(compressed_text, version=1):
    text = ''
    while compressed_text:
        idx = compressed_text.find('(')
        if idx == -1:
            return text + compressed_text
        text += compressed_text[:idx]
        compressed_text = compressed_text[idx+1:]
        idx = compressed_text.index('x')
        pattern_length = int(compressed_text[:idx])
        compressed_text = compressed_text[idx+1:]
        idx = compressed_text.index(')')
        repeats = int(compressed_text[:idx])
        compressed_text = compressed_text[idx+1:]
        pattern = compressed_text[:pattern_length]
        compressed_text = compressed_text[pattern_length:]
        if version == 2 and '(' in pattern:
            compressed_text = (pattern * repeats) + compressed_text
        else:
            text += pattern * repeats
    return text


def decompress_v2_len_only(compressed_text):
    if not compressed_text:
        return 0
    if compressed_text[0] != '(':
        return 1 + decompress_v2_len_only(compressed_text[1:])
    x_idx = compressed_text.index('x')
    pattern_length = int(compressed_text[1:x_idx])
    rp_idx = compressed_text.index(')')
    repeats = int(compressed_text[x_idx+1:rp_idx])
    pattern = compressed_text[rp_idx+1:rp_idx+1+pattern_length]
    return repeats * decompress_v2_len_only(pattern) \
        + decompress_v2_len_only(compressed_text[rp_idx+1+pattern_length:])


# part 1 examples
assert decompress('ADVENT') == 'ADVENT'
assert decompress('A(1x5)BC') == 'ABBBBBC'
assert decompress('(3x3)XYZ') == 'XYZXYZXYZ'
assert decompress('A(2x2)BCD(2x2)EFG') == 'ABCBCDEFEFG'
assert decompress('(6x1)(1x3)A') == '(1x3)A'
assert decompress('X(8x2)(3x3)ABCY') == 'X(3x3)ABC(3x3)ABCY'

# part 2 examples
assert decompress('(3x3)XYZ', version=2) == 'XYZXYZXYZ'
assert decompress('X(8x2)(3x3)ABCY', version=2) == 'XABCABCABCABCABCABCY'
assert decompress('(27x12)(20x12)(13x14)(7x10)(1x12)A', version=2) \
    == 'A' * 241920
assert len(
    decompress(
        '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN',
        version=2
    )) == 445

# part 2 fast
assert decompress_v2_len_only('ADVENT') == len('ADVENT')
assert decompress_v2_len_only('A(1x5)BC') == len('ABBBBBC')
assert decompress_v2_len_only('(3x3)XYZ') == len('XYZXYZXYZ')
assert decompress_v2_len_only('A(2x2)BCD(2x2)EFG') == len('ABCBCDEFEFG')
assert decompress_v2_len_only('(6x1)(1x3)A') == 3
assert decompress_v2_len_only('X(8x2)(3x3)ABCY') == 20
assert decompress_v2_len_only('(3x3)XYZ') == len('XYZXYZXYZ')
assert decompress_v2_len_only('X(8x2)(3x3)ABCY') == len('XABCABCABCABCABCABCY')
assert decompress_v2_len_only('(27x12)(20x12)(13x14)(7x10)(1x12)A') == 241920
assert decompress_v2_len_only(
        '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN') == 445

compressed_text = open(0).read().strip()
print('part 1: ', len(decompress(compressed_text)))
print('part 2: ', decompress_v2_len_only(compressed_text))
