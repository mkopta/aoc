#!/usr/bin/env python3
chars = {}
for line in open(0):
    for i, c in enumerate(line.strip()):
        if i not in chars:
            chars[i] = {}
        chars[i][c] = chars[i].get(c, 0) + 1
part1_message, part2_message = [], []
for i in range(max(chars.keys()) + 1):
    least_common_char, most_common_char = None, None
    lowest_frequency, highest_frequency = float('inf'), 0
    for char, frequency in chars[i].items():
        if frequency > highest_frequency:
            highest_frequency = frequency
            most_common_char = char
        if frequency < lowest_frequency:
            lowest_frequency = frequency
            least_common_char = char
    part1_message.append(most_common_char)
    part2_message.append(least_common_char)
print('part1:', ''.join(part1_message))
print('part2:', ''.join(part2_message))
