#!/usr/bin/env python


def load_instructions(filename):
    instructions = []
    for s in open(0).read().strip().split(', '):
        turn = s[0]
        steps = int(s[1:])
        instructions.append((turn, steps))
    return instructions


def manhattan_distance(cord1, cord2):
    dist = cord1 - cord2
    return abs(dist.real) + abs(dist.imag)


instructions = load_instructions(0)
current = 0 + 0j  # x, y
heading = 0 + 1j  # north
visited = {current}
part2 = None
for turn, steps in instructions:
    match turn:
        case 'L': heading *= 1j
        case 'R': heading *= -1j
    for _ in range(steps):
        current += heading
        if part2 is None and current in visited:
            part2 = current
        visited.add(current)
print('part1:', int(manhattan_distance(current, 0j)))
print('part2:', int(manhattan_distance(part2, 0j)))
