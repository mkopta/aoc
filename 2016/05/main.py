#!/usr/bin/env python3
import curses
from hashlib import md5


def curses_init(stdscr):
    stdscr.clear()
    stdscr.refresh()
    curses.curs_set(0)


def main(stdscr):
    curses_init(stdscr)
    stdscr.addstr(1, 1, 'part1:')
    stdscr.addstr(3, 1, 'part2:')
    puzzle_input = 'cxdnnyjw'
    part1_password, part2_password = [], ['_'] * 8
    stdscr.addstr(1, 10, '________')
    stdscr.addstr(3, 10, '________')
    stdscr.refresh()
    i = 0
    while len(part1_password) != 8 or '_' in part2_password:
        i += 1
        md5hash = md5(f'{puzzle_input}{i}'.encode()).hexdigest()
        if not md5hash.startswith('00000'):
            continue
        if len(part1_password) < 8:
            part1_password.append(md5hash[5])
            stdscr.addstr(
                1, 10, ''.join(
                    part1_password + ['_'] * (8 - len(part1_password))))
        if md5hash[5] in ('0', '1', '2', '3', '4', '5', '6', '7') \
                and part2_password[int(md5hash[5])] == '_':
            part2_password[int(md5hash[5])] = md5hash[6]
            stdscr.addstr(3, 10, ''.join(part2_password))
        stdscr.refresh()
    stdscr.getch()

curses.wrapper(main)
