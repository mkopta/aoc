#!/usr/bin/env python3
import re


def find_first_abba(s):
    if len(s) < 4:
        return False
    for i in range(len(s) - 3):
        if s[i] != s[i+1] and s[i] == s[i+3] and s[i+1] == s[i+2]:
            return s[i:i+4]
    return False


def find_all_abas(s):
    if len(s) < 3:
        return []
    abas = []
    for i in range(len(s) - 2):
        if s[i] != s[i+1] and s[i] == s[i+2]:
            abas.append(s[i:i+3])
    return abas


def parse_address(address):
    pieces = re.split(r'\[|\]', address)
    outsides = pieces[::2]
    hypernets = pieces[1::2]
    return outsides, hypernets


def supports_tls(address):
    outsides, hypernets = parse_address(address)
    return any(map(find_first_abba, outsides)) \
        and not any(map(find_first_abba, hypernets))


def supports_ssl(address):
    outsides, hypernets = parse_address(address)
    for outside in outsides:
        for aba in find_all_abas(outside):
            bab = aba[1] + aba[0] + aba[1]
            for hypernet in hypernets:
                if bab in hypernet:
                    return True
    return False


tls_count, ssl_count = 0, 0
for line in open(0):
    address = line.strip()
    if supports_tls(address): tls_count += 1
    if supports_ssl(address): ssl_count += 1
print('part1:', tls_count)
print('part1:', ssl_count)
