#!/usr/bin/env python3


def create_display(h, w):
    display = []
    for _ in range(h):
        row = ['.' for _ in range(w)]
        display.append(row)
    return display


def read_instructions(filename):
    instructions = []
    for line in open(filename):
        instruction = line.strip().split()
        instructions.append(instruction)
    return instructions


def execute_rect(display, w, h):
    for y in range(h):
        for x in range(w):
            display[y][x] = '#'


def execute_row_rot(display, index, count):
    for _ in range(count):
        execute_row_rot_1(display, index)


def execute_row_rot_1(display, index):
    x, y = len(display[index]) - 1, index
    last = display[y][x]
    while x:
        display[y][x] = display[y][x - 1]
        x -= 1
    display[y][0] = last


def execute_col_rot(display, index, count):
    for _ in range(count):
        execute_col_rot_1(display, index)


def execute_col_rot_1(display, index):
    x, y = index, len(display) - 1
    last = display[y][x]
    while y:
        display[y][x] = display[y - 1][x]
        y -= 1
    display[0][x] = last


def execute_instructions(display, instructions):
    for instruction in instructions:
        execute_instruction(display, instruction)


def execute_instruction(display, instruction):
    if instruction[0] == 'rect':
        w, h = map(int, instruction[1].split('x'))
        execute_rect(display, w, h)
    elif instruction[0] == 'rotate':
        index = int(instruction[2].split('=')[1])
        count = int(instruction[-1])
        if instruction[1] == 'row':
            execute_row_rot(display, index, count)
        elif instruction[1] == 'column':
            execute_col_rot(display, index, count)


def show_display(display):
    for row in display:
        for el in row:
            print(el, end='')
        print()


def count_lit_pixels(display):
    count = 0
    for row in display:
        for el in row:
            if el == '#':
                count += 1
    return count
    

#display = create_display(3, 7)
#instructions = read_instructions('example')
display = create_display(6, 50)
instructions = read_instructions('input')
execute_instructions(display, instructions)
show_display(display)
print(count_lit_pixels(display))
