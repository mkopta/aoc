#!/usr/bin/env python3
class Bot:
    def __init__(self, bot_id):
        self.bot_id = bot_id
        self.pass_low_to = None
        self.pass_high_to = None
        self.values = []
        self.compares = set()

    def accept(self, value):
        self.values.append(value)
        self.compares.add(value)
        if len(self.values) == 2:
            self._emit()

    def _emit(self):
        low, high = sorted(self.values)
        self.values.clear()
        self.pass_low_to.accept(low)
        self.pass_high_to.accept(high)


class Output:
    def __init__(self, output_id):
        self.output_id = output_id
        self.values = []

    def accept(self, value):
        self.values.append(value)


def read_instructions():
    instructions = []
    for line in open(0):
        instruction = line.strip()
        instructions.append(instruction)
    return instructions


def create_objects(instructions):
    bots, outputs = {}, {}
    for instruction in instructions:
        if not instruction.startswith('bot '):
            continue
        words = instruction.split()
        bot_id = int(words[1])
        if bot_id not in bots:
            bots[bot_id] = Bot(bot_id)
        low_id = int(words[6])
        if words[5] == 'output':
            if low_id not in outputs:
                outputs[low_id] = Output(low_id)
            bots[bot_id].pass_low_to = outputs[low_id]
        elif words[5] == 'bot':
            if low_id not in bots:
                bots[low_id] = Bot(low_id)
            bots[bot_id].pass_low_to = bots[low_id]
        high_id = int(words[11])
        if words[10] == 'output':
            if high_id not in outputs:
                outputs[high_id] = Output(high_id)
            bots[bot_id].pass_high_to = outputs[high_id]
        elif words[10] == 'bot':
            if high_id not in bots:
                bots[high_id] = Bot(high_id)
            bots[bot_id].pass_high_to = bots[high_id]
    return bots, outputs


def run_values(instructions, bots, outputs):
    for instruction in instructions:
        if not instruction.startswith('value '):
            continue
        words = instruction.split()
        value = int(words[1])
        bot_id = int(words[5])
        bots[bot_id].accept(value)


instructions = read_instructions()
bots, outputs = create_objects(instructions)
run_values(instructions, bots, outputs)

for bot in bots.values():
    if bot.compares == {61, 17}:
        print(f'Part 1: {bot.bot_id}')
        break

print(
    'Part 2: '
    f'{outputs[0].values[0] * outputs[1].values[0] * outputs[2].values[0]}'
)
