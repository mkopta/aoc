#!/usr/bin/env python3
import sys
from copy import deepcopy
from itertools import combinations



def make_next_state(state, direction, items):
    current_floor = state['current_floor']
    new_state = deepcopy(state)
    for i in items:
        new_state['floors'][current_floor].remove(i)
    new_state['current_floor'] = current_floor + direction
    new_state['floors'][current_floor + direction].extend(items)
    new_state['steps'] += 1
    return new_state


def is_valid_floor(floor):
    microchips = {item for item in floor if item[-1] == 'M'}
    generators = {item for item in floor if item[-1] == 'G'}
    if not generators:
        return True
    for microchip in microchips:
        generator = microchip[:-1] + 'G'
        if generator not in generators:
            return False
    return True


def is_valid_state(state):
    for floor in state['floors'].values():
        if not is_valid_floor(floor):
            return False
    return True


def expand_state(state):
    new_states = []
    current_floor = state['current_floor']
    for item in state['floors'][current_floor]:
        if current_floor < 4:
            new_states.append(make_next_state(state, 1, [item]))
        if current_floor > 1:
            new_states.append(make_next_state(state, -1, [item]))
    for item1, item2 in combinations(state['floors'][current_floor], 2):
        if current_floor < 4:
            new_states.append(make_next_state(state, +1, [item1, item2]))
        if current_floor > 1:
            new_states.append(make_next_state(state, -1, [item1, item2]))
    return [s for s in new_states if is_valid_state(s)]


def is_end_state(state):
    for i in range(1, 4):
        if state['floors'][i]:
            return False
    return True


queue = []
init_state = {
    'steps': 0,
    'current_floor': 1,
    'floors': {
        1: ['HM', 'LM'],
        2: ['HG'],
        3: ['LG'],
        4: [],
    }
}

__init_state = [
    # steps
    0,
    # current_floor
    1,
    # floors of HG HM LG LM
    2, 1, 3, 1
]


queue.append(init_state)
while queue:
    state = queue.pop(0)
    for s in expand_state(state):
        if is_end_state(s):
            print(s)
            sys.exit(0)
        queue.append(s)
