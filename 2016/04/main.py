#!/usr/bin/env python3


def parse_room(room):
    lsb = room.index('[')
    checksum = room[lsb+1:room.index(']', lsb)]
    sector_id = int(room[:lsb].split('-')[-1])
    encrypted_name = '-'.join(room[:lsb].split('-')[:-1])
    return encrypted_name, sector_id, checksum


def calculate_checksum(encrypted_room_name):
    letters = {}
    for letter in encrypted_room_name:
        if letter == '-':
            continue
        letters[letter] = letters.get(letter, 0) + 1
    frequency = {}
    for letter, count in letters.items():
        if count not in frequency:
            frequency[count] = []
        frequency[count].append(letter)
        frequency[count].sort()
    checksum = []
    for _, letters in sorted(frequency.items(), reverse=True):
        checksum.extend(letters)
    return ''.join(checksum[:5])


def is_real_room(room):
    return calculate_checksum(room[0]) == room[2]


def decrypt_name(room):
    encrypted_name, sector_id, _ = room
    shift = (sector_id % 24)
    name = []
    for c in encrypted_name:
        if c == '-':
            name.append(' ')
            continue
        n = ord(c) + (sector_id % 26)
        if n > ord('z'):
            n -= 26
        name.append(chr(n))
    return ''.join(name)


rooms = list(map(parse_room, [line.strip() for line in open(0)]))
print('part1:', sum(room[1] for room in rooms if is_real_room(room)))
for room in rooms:
    if decrypt_name(room) == 'northpole object storage':
        print('part2:', room[1])
