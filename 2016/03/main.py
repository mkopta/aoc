#!/usr/bin/env python

def is_triangle_valid(triangle):
    a, b, c = triangle
    return a + b > c and a + c > b and c + b > a

lines = open(0).readlines()

part1_triangles = [tuple(map(int, l.split())) for l in lines]
print('part1:', len(tuple(filter(is_triangle_valid, part1_triangles))))

part2_triangles = []
for i in range(0, len(lines), 3):
    l1 = tuple(map(int, lines[i].split()))
    l2 = tuple(map(int, lines[i+1].split()))
    l3 = tuple(map(int, lines[i+2].split()))
    for col in (0, 1, 2):
        triangle = l1[col], l2[col], l3[col]
        part2_triangles.append(triangle)
print('part2:', len(tuple(filter(is_triangle_valid, part2_triangles))))
