#!/usr/bin/env python

def get_pin(keypad, moves, start):
    pos = start
    buttons = []
    for sequence in moves:
        for direction in sequence:
            prev_pos = pos
            match direction:
                case 'U': pos += 1j
                case 'D': pos -= 1j
                case 'R': pos += 1
                case 'L': pos -= 1
            if pos not in keypad:
                pos = prev_pos
        buttons.append(pos)
    return ''.join([str(keypad[pos]) for pos in buttons])


part1_keypad = {
    -1+1j: 1, +0+1j: 2, +1+1j: 3,
    -1+0j: 4, +0+0j: 5, +1+0j: 6,
    -1-1j: 7, +0-1j: 8, +1-1j: 9,
}

part2_keypad = {
                          +0+2j:  1,
              -1+1j:  2 , +0+1j:  3 , +1+1j:  4 ,
    -2+0j: 5, -1+0j:  6 , +0+0j:  7 , +1+0j:  8 , +2+0j: 9,
              -1-1j: 'A', +0-1j: 'B', +1-1j: 'C',
                          +0-2j: 'D',
}


moves = [l.strip() for l in open(0)]
print('part1:', get_pin(part1_keypad, moves, +0+0j))
print('part2:', get_pin(part2_keypad, moves, -2+0j))
