#!/usr/bin/env python3.11


def load_instructions(filename):
    instructions = []
    for line in open(filename):
        line = line.strip()
        instr = line[:3]
        if instr in ('hlf', 'tpl', 'inc'):
            args = [line[4:]]
        elif instr == 'jmp':
            args = [int(line[4:])]
        elif instr in ('jmp', 'jie', 'jio'):
            reg, offset = line[4:].split(',')
            args = [reg, int(offset)]
        instructions.append((instr, args))
    return instructions


def execute(instructions, registers):
    ip = 0  # instruction pointer
    while 0 <= ip < len(instructions):
        instr, args = instructions[ip]
        ip += 1
        match instr:
            case 'hlf': registers[args[0]] = registers[args[0]] // 2
            case 'tpl': registers[args[0]] = registers[args[0]] * 3
            case 'inc': registers[args[0]] += 1
            case 'jmp': ip += args[0] - 1
            case 'jie' if registers[args[0]] % 2 == 0: ip += args[1] - 1
            case 'jio' if registers[args[0]] == 1: ip += args[1] - 1


instructions = load_instructions(0)
registers = {'a': 0, 'b': 0}
execute(instructions, registers)
print('part1:', registers['b'])
registers = {'a': 1, 'b': 0}
execute(instructions, registers)
print('part2:', registers['b'])
