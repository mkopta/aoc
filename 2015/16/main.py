#!/usr/bin/env python3
import copy


def load_sues(filename):
    sues = {}
    for line in open(filename):
        line = line.replace('\n', '').replace(':', '').replace(',', '')
        words = line.split()
        sue_number = words[1]
        words = words[2:]
        sues[sue_number] = {}
        while words:
            what = words.pop(0)
            count = int(words.pop(0))
            sues[sue_number][what] = count
    return sues


def find_sue(sues, ticker_tape, fixed_retroencabulator=False):
    eliminated_sues = set()
    for what, count in ticker_tape.items():
        for sue in sues:
            if sue in eliminated_sues or what not in sues[sue]:
                continue
            if not fixed_retroencabulator:
                if sues[sue][what] != count:
                    eliminated_sues.add(sue)
            else:
                if what in ('cats', 'trees'):
                    if sues[sue][what] <= count:
                        eliminated_sues.add(sue)
                elif what in ('pomeranians', 'goldfish'):
                    if sues[sue][what] >= count:
                        eliminated_sues.add(sue)
                elif sues[sue][what] != count:
                        eliminated_sues.add(sue)
    return list(set(sues) - eliminated_sues)[0]


ticker_tape = {
    'children': 3,
    'cats': 7,
    'samoyeds': 2,
    'pomeranians': 3,
    'akitas': 0,
    'vizslas': 0,
    'goldfish': 5,
    'trees': 3,
    'cars': 2,
    'perfumes': 1}
sues = load_sues(0)
print('part1: ', find_sue(sues, ticker_tape))
print('part1: ', find_sue(sues, ticker_tape, fixed_retroencabulator=True))
