#!/usr/bin/env python3
import itertools


def parse_preferences(filename):
    preferences = {}
    for line in open(filename):
        line = line.rstrip().rstrip('.').split()
        a, b = line[0], line[-1]
        if a not in preferences:
            preferences[a] = {}
        preferences[a][b] = int(line[3]) * (1 if line[2] == 'gain' else -1)
    return preferences


def find_max_happiness(preferences):
    max_happiness = 0
    for permutation in itertools.permutations(preferences):
        permutation_happiness = 0
        for a, b in itertools.pairwise(permutation + (permutation[0],)):
            permutation_happiness += preferences[a][b] + preferences[b][a]
        max_happiness = max(max_happiness, permutation_happiness)
    return max_happiness


def add_myself(preferences):
    preferences['me'] = {}
    for person in preferences:
        preferences['me'][person] = 0
        preferences[person]['me'] = 0


preferences = parse_preferences(0)
print('part1: ', find_max_happiness(preferences))
add_myself(preferences)
print('part2: ', find_max_happiness(preferences))
