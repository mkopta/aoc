#!/usr/bin/env python3
from copy import deepcopy


def load_grid(filename):
    grid = []
    for line in open(filename):
        row = []
        for c in line[:-1]:
            row.append(0 if c == '.' else 1)
        grid.append(row)
    return grid


def count_lit_neighbors(r, c, grid):
    count = 0
    max_r, max_c = len(grid) - 1, len(grid[0]) - 1
    for _r in (r - 1, r, r + 1):
        for _c in (c - 1, c, c + 1):
            if (_r, _c) == (r, c) \
                    or _r < 0 or _r > max_r or _c < 0 or _c > max_c:
                continue
            count += grid[_r][_c]
    return count


def step(grid_a, grid_b):
    for r in range(len(grid_a)):
        for c in range(len(grid_a[r])):
            neighbors = count_lit_neighbors(r, c, grid_a)
            if grid_a[r][c] == 1:
                grid_b[r][c] = 1 if neighbors in (2, 3) else 0
            else:
                grid_b[r][c] = 1 if neighbors == 3 else 0
    return grid_b


def switch_corners_on(grid):
    max_r, max_c = len(grid) - 1, len(grid[0]) - 1
    grid[0][0] = grid[max_r][0] = grid[0][max_c] = grid[max_r][max_c] = 1


def animate(grid, number_of_steps, corners_on=False):
    if corners_on:
        switch_corners_on(grid)
    grid_a = deepcopy(grid)
    grid_b = deepcopy(grid)
    for _ in range(number_of_steps):
        step(grid_a, grid_b)
        grid_a, grid_b = grid_b, grid_a
        if corners_on:
            switch_corners_on(grid_a)
    return grid_a


def count_lit_lights(grid):
    count = 0
    for r in range(len(grid)):
        for c in range(len(grid[r])):
            count += grid[r][c]
    return count


grid = load_grid(0)
print('part1: ', count_lit_lights(animate(grid, 100)))
print('part2: ', count_lit_lights(animate(grid, 100, corners_on=True)))
