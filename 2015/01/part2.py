#!/usr/bin/env python
floor = 0
position = 0
for c in open(0).read().strip():
    position += 1
    if c == '(':
        floor += 1
    else:
        floor -= 1
    if floor < 0:
        print(position)
        break
