#!/usr/bin/env python3
import sys
from collections import deque


if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} LITERS < input')
    sys.exit(1)
liters = int(sys.argv[1])
containers = list(map(int, open(0).readlines()))
seen = set()
q = deque()
q.append((tuple(), 0))
solutions = []
while q:
    used_containers, used_capacity = q.pop()
    if used_capacity == liters:
        solutions.append([containers[c] for c in used_containers])
    for i in range(len(containers)):
        if i not in used_containers:
            next_used_containers = tuple(sorted(used_containers + (i,)))
            next_used_capacity = used_capacity + containers[i]
            next_state = next_used_containers, next_used_capacity
            if next_used_capacity <= liters and next_state not in seen:
                seen.add(next_state)
                q.append(next_state)
print('part1: ', len(solutions))
min_containers = min(map(len, solutions))
print('part2: ', len([s for s in solutions if len(s) == min_containers]))
