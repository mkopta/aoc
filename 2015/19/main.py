#!/usr/bin/env python3.11
from queue import PriorityQueue


def load_input(filename):
    replacements = []
    content = open(filename).read()
    raw_replacements, init_molecule = content.strip().split('\n\n')
    for line in raw_replacements.split('\n'):
        a, b = line.split(' => ')
        replacements.append((a, b))
    return replacements, init_molecule


def replace(replacements, molecule):
    molecules = set()
    for a, b in replacements:
        offset = 0
        while True:
            index = molecule.find(a, offset)
            if index == -1:
                break
            new = molecule[0:index] + b + molecule[index + len(a):]
            molecules.add(new)
            offset = index + 1
    return molecules


def find_fewest_number_of_steps_from_e_to_molecule(replacements, molecule):
    reverse_replacements = [(b, a) for a, b in replacements]
    seen = set()
    q = PriorityQueue()
    q.put((len(molecule), 0, molecule))
    while not q.empty():
        priority, steps, current_molecule = q.get()
        if current_molecule == 'e':
            return steps
        for next_molecule in replace(reverse_replacements, current_molecule):
            if next_molecule in seen:
                continue
            seen.add(next_molecule)
            q.put((len(next_molecule), steps + 1, next_molecule))


replacements, molecule = load_input(0)
print('part1: ', len(replace(replacements, molecule)))
print(
    'part2: ',
    find_fewest_number_of_steps_from_e_to_molecule(replacements, molecule))
