#!/usr/bin/env python3
directions = open(0).read().strip()
position = [0, 0, 0, 0]  # santa x, y ; robosanta x, y
visited_coordinates = [(0, 0)]
for direction in directions:
    if direction == '^':
        position[1] += 1
    elif direction == '>':
        position[0] += 1
    elif direction == 'v':
        position[1] -= 1
    elif direction == '<':
        position[0] -= 1
    visited_coordinates.append((position[0], position[1]))
    # taking turns
    position = [position[2], position[3], position[0], position[1]]
print(len(set(visited_coordinates)))
