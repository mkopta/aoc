#!/usr/bin/env python3
directions = open(0).read().strip()
x, y = 0, 0
visited_coordinates = [(x, y)]
for direction in directions:
    if direction == '^':
        y += 1
    elif direction == '>':
        x += 1
    elif direction == 'v':
        y -= 1
    elif direction == '<':
        x -= 1
    visited_coordinates.append((x, y))
print(len(set(visited_coordinates)))
