#!/usr/bin/env python3

def parse_circuit(filename):
    circuit = {}
    for line in open(filename):
        left, wire = line.split(' -> ')
        wire = wire.strip()
        if left.isdigit():
            circuit[wire] = int(left)
        else:
            circuit[wire] = []
            for l in left.split():
                if l.isdigit():
                    circuit[wire].append(int(l))
                else:
                    circuit[wire].append(l)
    return circuit


def signal(w, c):
    if type(w) is int:
        return w
    if type(w) is str:
        x = signal(c[w], c)
        c[w] = x
        return x
    if type(w) is list:
        if len(w) == 1:
            return signal(c[w[0]], c)
        if w[0] == 'NOT':
            return ~signal(w[1], c) & (2**16-1)
        if w[1] == 'AND':
            return signal(w[0], c) & signal(w[2], c) & (2**16-1)
        if w[1] == 'OR':
            return signal(w[0], c) | signal(w[2], c) & (2**16-1)
        if w[1] == 'LSHIFT':
            return signal(w[0], c) << signal(w[2], c) & (2**16-1)
        if w[1] == 'RSHIFT':
            return signal(w[0], c) >> signal(w[2], c) & (2**16-1)
    raise Exception('xxx')


circuit = parse_circuit(0)
circuit['b'] = 956
print(signal('a', circuit))
