#!/usr/bin/env python3.11


def calc_best_qe(packages, number_of_groups=3):
    packages = tuple(sorted(packages, reverse=True))
    group_weight = sum(packages) // number_of_groups
    q = [((p,), p, p) for p in packages]
    seen = set()
    group1 = None
    qe = None
    while q:
        s, ssum, sqe = q.pop()
        if ssum > group_weight \
                or (group1 and len(s) > len(group1)) \
                or (qe and sqe > qe):
            continue
        elif ssum == group_weight:
            if not group1 or sqe < qe:
                group1 = s
                qe = sqe
        else:
            if group1:
                packages_remaining = len(group1) - (len(s) + 1)
                if packages_remaining < 0:
                    continue
                highest_addition = sum(packages[:packages_remaining])
            for p in packages:
                if p in s:
                    continue
                ns = tuple(sorted(s + (p,)))
                if ns in seen:
                    continue
                seen.add(ns)
                nssum = ssum + p
                nsqe = sqe * p
                if group1 and (nssum + highest_addition) < group_weight:
                    continue
                q.append((ns, ssum + p, sqe * p))
    return qe


packages = list(map(int, open(0).readlines()))
print('part1:', calc_best_qe(packages, number_of_groups=3))
print('part2:', calc_best_qe(packages, number_of_groups=4))
