#!/usr/bin/env python3

def look_and_say(number):
    new_number = ''
    i = 0
    while i < len(number):
        n = number[i]
        j = i
        while j < len(number) and number[j] == n:
            j += 1
        new_number += f'{j - i}{n}'
        i = j
    return new_number


assert look_and_say('1') == '11'
assert look_and_say('11') == '21'
assert look_and_say('21') == '1211'
assert look_and_say('1211') == '111221'
assert look_and_say('111221') == '312211'
number = '1113222113'
for _ in range(40):
    number = look_and_say(number)
print(len(number))
for _ in range(10):
    number = look_and_say(number)
print(len(number))
