#!/usr/bin/env python3
cities = set()
distances = {}
for line in open(0):
    city_a, _, city_b, _, dist = line.strip().split(' ')
    cities |= {city_a, city_b}
    distances[(city_a, city_b)] = int(dist)
    distances[(city_b, city_a)] = int(dist)
max_total_distance = 0
pqueue = [(0, [city]) for city in cities]
while pqueue:
    distance_traveled, cities_visited = pqueue.pop()
    if len(cities_visited) == len(cities):
        max_total_distance = max(max_total_distance, distance_traveled)
        break
    for city in cities:
        if city not in cities_visited:
            pqueue.append((
                distance_traveled + distances[(cities_visited[-1], city)],
                cities_visited[:] + [city]))
    pqueue.sort()
print(max_total_distance)
