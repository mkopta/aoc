#!/usr/bin/env python3
from queue import PriorityQueue

cities = set()
distances = {}
for line in open(0):
    city_a, _, city_b, _, dist = line.strip().split(' ')
    cities |= {city_a, city_b}
    distances[(city_a, city_b)] = int(dist)
    distances[(city_b, city_a)] = int(dist)
pqueue = PriorityQueue()
for city in cities:
    pqueue.put((0, [city]))
while pqueue.qsize() > 0:
    distance_traveled, cities_visited = pqueue.get()
    current_city = cities_visited[-1]
    if len(cities_visited) == len(cities):
        print(distance_traveled)
        break
    for city in cities:
        if city not in cities_visited:
            pqueue.put((
                distance_traveled + distances[(current_city, city)],
                cities_visited[:] + [city]))
