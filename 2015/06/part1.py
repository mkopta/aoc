#!/usr/bin/env python3
lights = set()
for instruction in open(0):
    instruction = instruction.strip()
    parts = list(instruction.split())
    w = parts.pop(0)
    toggle, turn_on, turn_off = False, False, False
    if w == 'turn':
        w = parts.pop(0)
        if w == 'on':
            turn_on = True
        elif w == 'off':
            turn_off = True
    elif w == 'toggle':
        toggle = True
    start_a, start_b = map(int, parts.pop(0).split(','))
    end_a, end_b = map(int, parts[-1].split(','))
    for a in range(start_a, end_a + 1):
        for b in range(start_b, end_b + 1):
            light = (a, b)
            if light not in lights and (turn_on or toggle):
                lights.add(light)
            elif light in lights and (turn_off or toggle):
                lights.remove(light)
print(len(lights))
