#!/usr/bin/env python3.11
from copy import copy
from queue import PriorityQueue
from collections import namedtuple

Boss = namedtuple('Boss', ['hp', 'damage'])
Player = namedtuple('Player', ['hp', 'mana'])
Effect = namedtuple('Effect', ['type', 'timer'])
instants = {'magic-missile': 53, 'drain': 73}
effects = {'shield': (113, 6), 'poison': (173, 6), 'recharge': (229, 5)}


def eval_instant(instant_type, player, boss):
    match instant_type:
        case 'magic-missile': return player, Boss(boss.hp - 4, boss.damage)
        case 'drain':
            return Player(player.hp + 2, player.mana), \
                Boss(boss.hp - 2, boss.damage)


def eval_effect(effect, player, boss):
    match effect.type:
        case 'shield': return player, boss
        case 'poison': return player, Boss(boss.hp - 3, boss.damage)
        case 'recharge': return Player(player.hp, player.mana + 101), boss


def find_min_mana_expense(player, boss, difficulty='easy'):
    q = PriorityQueue()
    q.put((boss.hp, 'player', player, boss, tuple(), 0))
    min_mana_spent = float('inf')
    while q.qsize() > 0:
        _, turn, player, boss, active_effects, mana_spent = q.get()
        if mana_spent >= min_mana_spent:
            continue
        if difficulty == 'hard':
            player = Player(player.hp - 1, player.mana)
            if player.hp <= 0:
                continue
        for effect in active_effects:
            player, boss = eval_effect(effect, player, boss)
        active_effects = tuple(
            Effect(e.type, e.timer - 1) for e in active_effects if e.timer > 1)
        if boss.hp <= 0:
            min_mana_spent = min(min_mana_spent, mana_spent)
            continue
        if turn == 'player':
            for instant, cost in instants.items():
                if cost > player.mana:
                    continue
                _mana_spent = mana_spent + cost
                _player = Player(player.hp, player.mana - cost)
                _player, _boss = eval_instant(instant, _player, boss)
                if _boss.hp <= 0:
                    min_mana_spent = min(min_mana_spent, _mana_spent)
                    continue
                q.put((
                    _boss.hp, 'boss', _player, _boss, active_effects,
                    _mana_spent))
            for effect, (cost, timer) in effects.items():
                if effect in (e.type for e in active_effects):
                    continue
                if cost > player.mana:
                    continue
                _player = Player(player.hp, player.mana - cost)
                q.put((
                    boss.hp, 'boss', _player, boss,
                    active_effects + (Effect(effect, timer),),
                    mana_spent + cost))
            # technically should consider no-action too..
        elif turn == 'boss':
            has_shield = [e for e in active_effects if e.type == 'shield']
            damage = boss.damage - (7 if has_shield else 0)
            player = Player(player.hp - damage, player.mana)
            if player.hp <= 0:
                continue
            q.put((
                boss.hp, 'player', player, boss, active_effects, mana_spent))
    return min_mana_spent


player, boss = Player(hp=50, mana=500), Boss(hp=58, damage=9)
print('part1: ', find_min_mana_expense(player, boss))
print('part2: ', find_min_mana_expense(player, boss, difficulty='hard'))
