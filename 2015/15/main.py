#!/usr/bin/env python3
import re


def load_ingredients(filename):
    ingredients = {}
    for line in open(filename):
        ingredient = line[:line.index(':')]
        numbers = re.findall('-?\d+', line)
        ingredients[ingredient] = {
            'capacity': int(numbers[0]),
            'durability': int(numbers[1]),
            'flavor': int(numbers[2]),
            'texture': int(numbers[3]),
            'calories': int(numbers[4])}
    return ingredients


def find_max_score(ingredients, target_calories=None):
    max_score = 0
    if len(ingredients) == 2:
        a, b = ingredients.values()
        for i in range(101):
            j = 100 - i
            if target_calories is not None:
                calories = a['calories'] * i + b['calories'] * j
                if calories != target_calories:
                    continue
            capacity = a['capacity'] * i + b['capacity'] * j
            durability = a['durability'] * i + b['durability'] * j
            flavor = a['flavor'] * i + b['flavor'] * j
            texture = a['texture'] * i + b['texture'] * j
            if capacity < 0 or durability < 0 or flavor < 0 or texture < 0:
                continue
            score = capacity * durability * flavor * texture
            max_score = max(max_score, score)
    elif len(ingredients) == 4:
        a, b, c, d = ingredients.values()
        for i in range(101):
            for j in range(101 - i):
                for k in range(101 - i - j):
                    l = 100 - i - j - k
                    if target_calories is not None:
                        calories = sum(
                            i['calories'] * n
                            for i, n in ((a, i), (b, j), (c, k), (d, l)))
                        if calories != target_calories:
                            continue
                    capacity = sum(
                        i['capacity'] * n
                        for i, n in ((a, i), (b, j), (c, k), (d, l)))
                    durability = sum(
                        i['durability'] * n
                        for i, n in ((a, i), (b, j), (c, k), (d, l)))
                    flavor = sum(
                        i['flavor'] * n
                        for i, n in ((a, i), (b, j), (c, k), (d, l)))
                    texture = sum(
                        i['texture'] * n
                        for i, n in ((a, i), (b, j), (c, k), (d, l)))
                    if capacity < 0 or durability < 0 or flavor < 0 or texture < 0:
                        continue
                    score = capacity * durability * flavor * texture
                    max_score = max(max_score, score)
    return max_score


ingredients = load_ingredients(0)
print('part1: ', find_max_score(ingredients))
print('part2: ', find_max_score(ingredients, target_calories=500))
