#!/usr/bin/env python3


def contains_increasing_straight(password):
    for i in range(len(password) - 2):
        a, b, c = password[i:i+3]
        if ord(a) == ord(b) - 1 == ord(c) - 2:
            return True
    return False


def contains_forbidden_letters(password):
    return 'i' in password \
        or 'o' in password \
        or 'l' in password


def contains_two_pairs(password):
    i = pairs = 0
    while i < len(password) - 1:
        if password[i] == password[i + 1]:
            pairs += 1
            if pairs == 2:
                return True
            i += 2
        else:
            i += 1
    return False


def is_valid_password(password):
    return contains_increasing_straight(password) \
        and not contains_forbidden_letters(password) \
        and contains_two_pairs(password)


def increment_letter(c):
    if c == 'z':
        return 'a', True
    return chr(ord(c) + 1), False


def increment_string(s):
    s = list(s)
    for i in range(len(s) - 1, -1, -1):
        s[i], carry = increment_letter(s[i])
        if not carry:
            break
    return ''.join(s)


def increment_beyond_forbidden_letter(password):
    password = list(password)
    forbidden_letter = [l for l in ('i', 'o', 'l') if l in password][0]
    idx = password.index(forbidden_letter)
    password[idx], _ = increment_letter(forbidden_letter)
    if idx + 1 < len(password):
        for i in range(idx + 1, len(password)):
            password[i] = 'a'
    return ''.join(password)


def increment_password(p):
    p = increment_string(p)
    while not is_valid_password(p):
        if contains_forbidden_letters(p):
            p = increment_beyond_forbidden_letter(p)
        else:
            p = increment_string(p)
    return p
    

puzzle_input = 'hxbxwxba'
part1 = increment_password(puzzle_input)
print('part1: ', part1)
part2 = increment_password(part1)
print('part2: ', part2)
