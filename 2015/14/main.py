#!/usr/bin/env python3
import re


def parse_reindeers(filename):
    reindeers = {}
    for line in open(filename):
        name = line.split()[0]
        speed, flying, resting = map(int, re.findall('\d+', line))
        reindeers[name] = speed, flying, resting
    return reindeers


def distances_at(t, reindeers):
    distances = {}
    for name, (speed, flying, resting) in reindeers.items():
        distance = (
            (t // (flying + resting)) * (flying * speed)
            + min(flying, t % (flying + resting)) * speed)
        distances[name] = distance
    return distances


reindeers = parse_reindeers(0)
print('part1: ', max(distances_at(2503, reindeers).values()))
points = {name: 0 for name in reindeers}
for t in range(1, 2504):
    distances = distances_at(t, reindeers)
    max_distance = max(distances.values())
    for name, distance in distances.items():
        if distance == max_distance:
            points[name] += 1
print('part2: ', max(points.values()))
