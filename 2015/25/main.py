#!/usr/bin/env python3.11

def get_code(row, col):
    prev_val = 20151125
    i = 2
    while True:
        for j in range(1, i + 1):
            r = i - j + 1
            c = j
            val = (prev_val * 252533) % 33554393
            if (r, c) == (row, col):
                return val
            prev_val = val
        i += 1

print('part1:', get_code(2981, 3075))
