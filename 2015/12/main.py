#!/usr/bin/env python3
import re
import json


def sum_numbers(doc):
    if type(doc) is int:
        return doc
    elif type(doc) is list:
        return sum(map(sum_numbers, doc))
    elif type(doc) is dict:
        if 'red' in doc.values():
            return 0
        return sum(map(sum_numbers, doc.values()))
    return 0


content = open(0).read()
print('part1: ', sum(map(int, re.findall('-?\d+', content))))
doc = json.loads(content)
print('part2: ', sum_numbers(doc))
