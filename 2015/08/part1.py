#!/usr/bin/env python3
code_len = mem_len = 0
for s in open(0):
    s = list(s.strip())
    code_len += len(s)
    s = s[1:-1]
    while s:
        c = s.pop(0)
        if c == '\\':
            c = s.pop(0)
            if c == 'x':
                s.pop(0)
                s.pop(0)
        mem_len += 1
print(code_len - mem_len)
