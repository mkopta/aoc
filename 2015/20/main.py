#!/usr/bin/env python3.11
import math


def find_divisors(n):
    divisors = set()
    for i in range(1, int(math.sqrt(n)) + 1):
        if n % i == 0:
            divisors.add(i)
            divisors.add(n // i)
    return divisors


def part1_calculation(N):
    """ takes ~17s """
    n = 1
    while True:
        if (sum(find_divisors(n)) * 10) >= N:
            return n
        n += 1


def part1_simulation(N):
    """ takes ~24s """
    houses = {i: 0 for i in range(1, N//10 + 1)}
    for i in range(1, N//10 + 1):
        for j in range(i, N//10 + 1, i):
            houses[j] += i * 10
    for i in range(1, N//10 + 1):
        if houses[i] >= N:
            return i


def part2_simulation(N):
    """ takes ~10s """
    houses = {i: 0 for i in range(1, N//10 + 1)}
    for i in range(1, N//10 + 1):
        count = 0
        for j in range(i, N//10 + 1, i):
            houses[j] += i * 11
            count += 1
            if count > 50:
                break
    for i in range(1, N//10 + 1):
        if houses[i] >= N:
            return i


def part2_calculation(N):
    """ takes ~20s """
    n = 1
    while True:
        if (sum(d for d in find_divisors(n) if n / d <= 50) * 11) >= N:
            return n
        n += 1


N = 34000000
print('part1:', part1_calculation(N))
#print('part1:', part1_simulation(N))
print('part2:', part2_simulation(N))
#print('part2:', part2_calculation(N))
