#!/usr/bin/env python3
from math import ceil
from dataclasses import dataclass


@dataclass
class Item:
    name: str
    cost: int
    damage: int
    armor: int


@dataclass
class Character:
    name: str
    hp: int
    damage: int
    armor: int


WEAPONS = [
    Item('dagger',       8, 4, 0),
    Item('shortsword',  10, 5, 0),
    Item('warhammer',   25, 6, 0),
    Item('longsword',   40, 7, 0),
    Item('greataxe',    74, 8, 0)]
ARMORS = [
    Item('leather',     13, 0, 1),
    Item('chainmail',   31, 0, 2),
    Item('splintmail',  53, 0, 3),
    Item('bandedmail',  75, 0, 4),
    Item('platemail',  102, 0, 5)]
RINGS = [
    Item('damage+1',    25, 1, 0),
    Item('damage+2',    50, 2, 0),
    Item('damage+3',   100, 3, 0),
    Item('defense+1',   20, 0, 1),
    Item('defense+2',   40, 0, 2),
    Item('defense+3',   80, 0, 3)]


def does_player_win(player, enemy):
    turns_to_kill_enemy = ceil(
        enemy.hp / max(player.damage - enemy.armor, 1))
    turns_before_player_dies = ceil(
        player.hp / max(enemy.damage - player.armor, 1))
    return turns_to_kill_enemy <= turns_before_player_dies


def enumerate_combinations(weapons, armors, rings):
    for weapon in weapons:
        for armor in armors + [None]:
            for ring1 in rings + [None]:
                for ring2 in rings + [None]:
                    if ring1 == ring2:
                        continue
                    player = Character('player', 100, 0, 0)
                    cost = 0
                    for item in (weapon, armor, ring1, ring2):
                        if item:
                            player.damage += item.damage
                            player.armor += item.armor
                            cost += item.cost
                    yield player, cost


enemy = Character('enemy', 103, 9, 2)
min_cost_to_win = float('inf')
max_cost_to_lose = 0
for player, cost in enumerate_combinations(WEAPONS, ARMORS, RINGS):
    if does_player_win(player, enemy):
        min_cost_to_win = min(min_cost_to_win, cost)
    else:
        max_cost_to_lose = max(max_cost_to_lose, cost)
print('part1:', min_cost_to_win)
print('part2:', max_cost_to_lose)
