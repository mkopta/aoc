#!/usr/bin/env python3
area = 0
for line in open(0).readlines():
    line = line.strip()
    l, w, h = map(int, line.split('x'))
    lw, wh, hl = l * w, w * h, h * l
    area += min(lw, wh, hl) + (2 * lw) + (2 * wh) + (2 * hl)
print(area)
