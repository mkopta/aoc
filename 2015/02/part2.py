#!/usr/bin/env python3
length = 0
for line in open(0):
    line = line.strip()
    l, w, h = map(int, line.split('x'))
    a, b = sorted([l, w, h])[:2]
    length += a + a + b + b + (l * w * h)
print(length)
