#!/usr/bin/env python3


def is_nice_string(s):
    for i in range(len(s) - 3):
        pair = s[i:i + 2]
        rest = s[i + 2:]
        if pair in rest:
            break
    else:
        return False
    for i in range(len(s) - 2):
        if s[i] == s[i + 2]:
            break
    else:
        return False
    return True


count = 0
for s in open(0).read().splitlines():
    if is_nice_string(s):
        #print(f'String {s} is nice.')
        count += 1
    else:
        #print(f'String {s} is naughty.')
        pass
print(count)
