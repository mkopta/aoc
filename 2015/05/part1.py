#!/usr/bin/env python3


def is_nice_string(s):
    if len([c for c in s if c in 'aeiou']) < 3:
        return False
    for a, b in zip(s, s[1:]):
        if a == b:
            break
    else:
        return False
    for bad_string in {'ab', 'cd', 'pq', 'xy'}:
        if bad_string in s:
            return False
    return True


count = 0
for s in open(0).read().splitlines():
    if is_nice_string(s):
      count += 1
print(count)
