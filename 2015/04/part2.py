#!/usr/bin/env python3.11
import hashlib

secret_key = 'ckczppom'
i = 1
while True:
    h = hashlib.md5((f'{secret_key}{i}').encode()).hexdigest()
    if h.startswith('000000'):
        print(i)
        break
    i += 1
