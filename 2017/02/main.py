#!/usr/bin/env python
from itertools import permutations
part1_checksum, part2_checksum = 0, 0
for line in open(0):
    numbers = [int(n) for n in line.split()]
    part1_checksum += max(numbers) - min(numbers)
    for a, b in permutations(numbers, 2):
        if a == b or a < b:
            continue
        if a % b == 0:
            part2_checksum += a // b
            break
print('part 1: ', part1_checksum)
print('part 2: ', part2_checksum)
