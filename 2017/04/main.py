#!/usr/bin/env python3
part1_count, part2_count = 0, 0
for passphrase in open(0):
    words = passphrase.strip().split()
    sorted_words = [''.join(sorted(w)) for w in words]
    if len(words) == len(set(words)):
        part1_count += 1
    if len(words) == len(set(sorted_words)):
        part2_count += 1
print('part 1:', part1_count)
print('part 2:', part2_count)
