#!/usr/bin/env python

def part1(text):
    the_sum = 0
    last_letter = text[0]
    for l in text[1:]:
        if l == last_letter:
            the_sum += int(l)
        last_letter = l
    if text[0] == text[-1]:
        the_sum += int(text[0])
    return the_sum


def part2(text):
    the_sum = 0
    for i in range(len(text)):
        if text[i] == text[(i + len(text) // 2) % len(text)]:
            the_sum += int(text[i])
    return the_sum


assert part1('1122') == 3
assert part1('1111') == 4
assert part1('1234') == 0
assert part1('91212129') == 9

assert part2('1212') == 6
assert part2('1221') == 0
assert part2('123425') == 4
assert part2('123123') == 12
assert part2('12131415') == 4

text = open('input').read().strip()

print('part1:', part1(text))
print('part2:', part2(text))
