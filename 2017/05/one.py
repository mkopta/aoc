#!/usr/bin/env python3
ic = 0  # instruction counter
ip = 0  # instruction pointer
instructions = [int(n) for n in open(0)]
while ip < len(instructions):
    next_ip = ip + instructions[ip]
    instructions[ip] += 1
    ip = next_ip
    ic += 1
print(ic)
