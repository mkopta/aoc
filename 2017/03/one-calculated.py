#!/usr/bin/env python3
import math


def resolve_coordinates(n):
    cube_size = int(math.sqrt(n))
    if cube_size % 2 == 0:
        cube_size -= 1
    radius = int(cube_size / 2)
    rest = n - (cube_size * cube_size)
    start = (-radius, radius)  # y, x
    if rest == 0:
        return start
    outer_side_size = cube_size + 1
    # going bottom right -> top right
    if rest <= outer_side_size:
        return (-radius - 1 + rest, radius + 1)
    rest -= outer_side_size
    # going top right -> top left
    if rest <= outer_side_size:
        return (radius + 1, radius + 1 - rest)
    rest -= outer_side_size
    # going top left -> bottom left
    if rest <= outer_side_size:
        return (radius + 1 - rest, -radius - 1)
    rest -= outer_side_size
    # going bottom left -> bottom right
    return (- radius - 1, -radius - 1 + rest)


c = resolve_coordinates(368078)
print(abs(c[0]) + abs(c[1]))
