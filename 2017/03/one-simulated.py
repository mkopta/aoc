#!/usr/bin/env python3

def print_grid(grid):
    min_x = min(x for x, _ in grid)
    max_x = max(x for x, _ in grid)
    min_y = min(y for _, y in grid)
    max_y = max(y for _, y in grid)
    max_n_len = len(str(max(grid.values())))
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if (x, y) in grid:
                print(f'{grid[(x, y)]:={max_n_len}}', end=' ')
            else:
                print(' ' * max_n_len, end=' ')
        print()


# (x, y)
grid = {(0, 0): 1}
lpx, lpy = (0, 0)  # last position
for _ in range(368078 - 1):
    right, left = (lpx + 1, lpy), (lpx - 1, lpy)
    top, bottom = (lpx, lpy - 1), (lpx, lpy + 1)
    if left in grid and not top in grid:
        npx, npy = top
    elif bottom in grid:
        npx, npy = left
    elif right in grid:
        npx, npy = bottom
    else:
        npx, npy = right
    grid[(npx, npy)] = grid[(lpx, lpy)] + 1
    lpx, lpy = npx, npy

#print_grid(grid)
print(abs(lpx) + abs(lpy))
