#!/usr/bin/env python3

def adjacent(position):
    x, y = position
    return (
        (x + 1, y    ),  # →
        (x + 1, y - 1),  # ↗
        (x    , y - 1),  # ↑
        (x - 1, y - 1),  # ↖
        (x - 1, y    ),  # ←
        (x - 1, y + 1),  # ↙
        (x    , y + 1),  # ↓
        (x + 1, y + 1),  # ↘
    )

# (x, y)
grid = {(0, 0): 1}
lpx, lpy = (0, 0)  # last position
while True:
    right, left = (lpx + 1, lpy), (lpx - 1, lpy)
    top, bottom = (lpx, lpy - 1), (lpx, lpy + 1)
    if left in grid and not top in grid:
        npx, npy = top
    elif bottom in grid:
        npx, npy = left
    elif right in grid:
        npx, npy = bottom
    else:
        npx, npy = right
    value = 0
    for adj in adjacent((npx, npy)):
        value += grid.get(adj, 0)
    if value > 368078:
        print(value)
        break
    grid[(npx, npy)] = value
    lpx, lpy = npx, npy
