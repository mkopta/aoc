#!/usr/bin/env python3
import sys

box_ids = []
for line in open(0):
    box_ids.append(line.strip())
for i, box_id1 in enumerate(box_ids):
    for j, box_id2 in enumerate(box_ids):
        if i == j:
            continue
        differences = 0
        common = []
        for a, b in zip(box_id1, box_id2):
            if a != b:
                differences += 1
            else:
                common.append(a)
        if differences == 1:
            print(''.join(common))
            sys.exit(0)
