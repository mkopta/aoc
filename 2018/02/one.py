#!/usr/bin/env python3
unique_occurences_counts = {}
for line in open(0):
    occurences = {}
    for letter in line.strip():
        occurences[letter] = occurences.get(letter, 0) + 1
    for o in set(occurences.values()):
        unique_occurences_counts[o] = unique_occurences_counts.get(o, 0) + 1
print(unique_occurences_counts[2] * unique_occurences_counts[3])
