#!/usr/bin/env python3
import re


def read_claims():
    claims = {}
    for line in open(0):
        m = re.match(r'^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)\s*$', line)
        claim_id = int(m[1])
        margin_left = int(m[2])
        margin_top = int(m[3])
        width = int(m[4])
        height = int(m[5])
        claims[claim_id] = (margin_left, margin_top, width, height)
    return claims


def square_inches_of_fabric_claimed(claim_spec):
    square_inches = []
    ml, mt, w, h = claim_spec
    for row in range(mt + 1, mt + 1 + h):
        for col in range(ml + 1, ml + 1 + w):
            square_inches.append((row, col))
    return square_inches


claims = read_claims()
square_inches = {}
for claim_id, claim_spec in claims.items():
    for square_inch in square_inches_of_fabric_claimed(claim_spec):
        if square_inch not in square_inches:
            square_inches[square_inch] = []
        square_inches[square_inch].append(claim_id)
overlaping_claim_ids = set()
for si in square_inches:
    if len(square_inches[si]) > 1:
        for claim_id in square_inches[si]:
            overlaping_claim_ids.add(claim_id)
print(set(claims) - overlaping_claim_ids)
