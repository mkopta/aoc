#!/usr/bin/env python3
from itertools import cycle

changes = [int(line) for line in open(0)]
frequency = 0
occurences = {0: 1}
for change in cycle(changes):
    frequency += change
    occurences[frequency] = occurences.get(frequency, 0) + 1
    if occurences[frequency] == 2:
        print(frequency)
        break
